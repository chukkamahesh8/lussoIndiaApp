package com.lussoindia.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lussoindia.R;

import butterknife.BindView;
import ss.com.bannerslider.views.BannerSlider;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class HomeHolder extends AbstrctRecyclerViewholder {
    @BindView(R.id.banners)
    BannerSlider bannerSlider;

    @BindView(R.id.txt_type)
    TextView txtType;

    public TextView getTxtSeeMore() {
        return txtSeeMore;
    }

    @BindView(R.id.txt_see_more)
    TextView txtSeeMore;

    @BindView(R.id.rv_products)
    RecyclerView rvProducts;

    public LinearLayout getLlseeMore() {
        return llseeMore;
    }

    @BindView(R.id.ll_see_more)
    LinearLayout llseeMore;

    @BindView(R.id.im_see_more)
    ImageView imSeeMore;

    public BannerSlider getBannerSlider() {
        return bannerSlider;
    }

    public TextView getTxtType() {
        return txtType;
    }

    public RecyclerView getRvProducts() {
        return rvProducts;
    }

    public ImageView getImSeeMore() {
        return imSeeMore;
    }

    public HomeHolder(View itemView) {
        super(itemView);
    }
}
