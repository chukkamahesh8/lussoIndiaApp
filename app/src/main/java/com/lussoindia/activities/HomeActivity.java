package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.fragments.CategoriesDialog;
import com.lussoindia.holders.HomeHolder;
import com.lussoindia.holders.HomeProductsHolder;
import com.lussoindia.models.home.DisplayItems;
import com.lussoindia.models.home.HomeResponse;
import com.lussoindia.models.home.Products;
import com.lussoindia.models.keywordSearch.KeywordSearchResponse;
import com.lussoindia.network.NetworkConnectivity;
import com.lussoindia.network.RestApi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class HomeActivity extends BaseActivity implements TextWatcher, View.OnClickListener {
    android.support.v7.widget.Toolbar toolbar;
    ImageView txtSearch;
    @BindView(R.id.rv_home_list)
    RecyclerView rvHomeList;
    HomePageAdapter adapter;
    private String customerId = null;
    @BindView(R.id.txt_empty)
    TextView txtEmpty;
    Snackbar snackbar;
    @BindView(R.id.parentLayout)
    LinearLayout Parentlayout;
    EditText et_search;
    ImageView imCat, imMenu;
    private SharedPreferences sp = null;
    List<DisplayItems> homeDisplayItemsList = new ArrayList<>();
    TextView txtPageTitle;
    PopupMenu popup = null;
    ImageView imAppLogo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),
                R.drawable.ic_about);
        // toolbar.setOverflowIcon(drawable);
        customerId = sp.getString("customerId", "");//
        Boolean loginStatus = sp.getBoolean("loginStatus", false);//
        // Log.d("customerId__",customerId);
        // Log.d("h_loginStatus", loginStatus.toString());
        if (customerId.isEmpty() || customerId.equalsIgnoreCase("") || customerId == null) {
            sp.edit().putBoolean("loginStatus", false).commit();
        } else {
            Log.d("customerId__", customerId);
            sp.edit().putBoolean("loginStatus", true).commit();
        }
        // Log.d("homeCID", sp.getString("customerId", ""));
        toolbar = findViewById(R.id.toolbar);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("");
        txtPageTitle.setTextColor(getResources().getColor(R.color.colorAccent));
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo1);
       // imAppLogo.setBackgroundDrawable(getResources().getDrawable(R.drawable.newapplogo1));

        imMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup = new PopupMenu(HomeActivity.this, (View) imMenu);
                popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());
                if (sp.getBoolean("loginStatus", false)) {
                    if (popup.getMenu() != null) {
                        popup.getMenu().findItem(R.id.login).setVisible(false);
                        popup.getMenu().findItem(R.id.logout).setVisible(true);
                        popup.getMenu().findItem(R.id.my_cart).setVisible(true);
                        popup.getMenu().findItem(R.id.my_profile).setVisible(true);
                        popup.getMenu().findItem(R.id.my_orders).setVisible(true);
                    }
                } else {
                    popup.getMenu().findItem(R.id.login).setVisible(true);
                    popup.getMenu().findItem(R.id.logout).setVisible(false);
                    popup.getMenu().findItem(R.id.my_cart).setVisible(false);
                    popup.getMenu().findItem(R.id.my_profile).setVisible(false);
                    popup.getMenu().findItem(R.id.my_orders).setVisible(false);
                }


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = null;
                        switch (item.getItemId()) {

                            case R.id.my_cart:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(HomeActivity.this, CartActivity.class);
                                intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.my_profile:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(HomeActivity.this, MyProfileActivity.class);
                                intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.my_orders:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(HomeActivity.this, MyOrdersActivity.class);
                                // intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.login:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(HomeActivity.this, LoginActivity.class);
                                intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.logout:
                                sp.edit().putBoolean("loginStatus", false).commit();
                                sp.edit().putString("customerId", "").commit();
                                final Boolean loginStatus = sp.getBoolean("loginStatus", false);
                                Log.d("pd_loginStatus", loginStatus.toString());
                                popup.getMenu().findItem(R.id.login).setVisible(false);
                                popup.getMenu().findItem(R.id.logout).setVisible(true);
                                popup.getMenu().findItem(R.id.my_cart).setVisible(true);
                                popup.getMenu().findItem(R.id.my_profile).setVisible(true);
                                return true;


                            default:
                                return false;
                        }

                    }
                });

                popup.show();
            }
        });
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        et_search = (EditText) findViewById(R.id.et_search);
        txtSearch = (ImageView) findViewById(R.id.txt_search);
        //et_search.setText("essse");
        imCat = (ImageView) findViewById(R.id.im_cat);
        // imCat.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        //  txtSearch.setTextColor(getResources().getColor(R.color.colorAccent));
        txtSearch.setOnClickListener(this);

        //et_search.addTextChangedListener(this);
        adapter = new HomePageAdapter(this);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvHomeList.setLayoutManager(layoutManager);

        // customerId = sp.getString("customerId", "");

        imCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getSupportFragmentManager();
                CategoriesDialog fragment = new CategoriesDialog().newInstance("Services");
                //fragment.setListener(CreateEmployeeActivity.this);
                fragment.show(fm, "services_dialog");
            }
        });

        networkCheck();

    }

    private void networkCheck() {
        if (NetworkConnectivity.isAvailable(HomeActivity.this)) {

            if (TextUtils.isEmpty(customerId)) {
                getHomeList("");
            } else {
                getHomeList(customerId);
            }
        } else {
            snackbar = Snackbar
                    .make(Parentlayout, "No Internet Connection", Snackbar.LENGTH_LONG)
                    .setAction("Enable", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(dialogIntent);
                        }
                    });

            snackbar.show();
        }


    }

    private void getHomeList(String customerId) {
        showProgress();
        //Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeList();
        Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeList(customerId);
        getHomeList.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        homeDisplayItemsList = response.body().getDisplayItems();
                        txtEmpty.setVisibility(View.GONE);
                        rvHomeList.setVisibility(View.VISIBLE);
                        adapter.addItems(response.body().getDisplayItems());
                        rvHomeList.setAdapter(adapter);
                    } else {

                        txtEmpty.setVisibility(View.VISIBLE);
                        rvHomeList.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    @Override
    public void beforeTextChanged(CharSequence query, int start, int count, int after) {
        query = query.toString().toLowerCase();
        final List<DisplayItems> filteredList = new ArrayList<>();

        //callKeywordSearchService(query);
/*
        for (int i = 0; i < homeDisplayItemsList.size(); i++) {
            final List<Products> productList = new ArrayList<>();
            for (int j = 0; j < homeDisplayItemsList.get(i).getProducts().size(); j++) {
                final String text = homeDisplayItemsList.get(i).getProducts().get(j).getTitle();
                if (text.contains(query)) {
                    productList.add(homeDisplayItemsList.get(i).getProducts().get(j));
                }
                homeDisplayItemsList.get(i).setProducts(productList);
            }
        }

        adapter.addItems(filteredList);
        rvHomeList.setAdapter(adapter);
        adapter.notifyDataSetChanged();// data set changed*/
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_search:
                keywordSearch();
                break;
        }

    }

    private void keywordSearch() {
        String keyword = et_search.getText().toString().trim();
        if (TextUtils.isEmpty(et_search.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Please enter search keyword", Toast.LENGTH_SHORT).show();
        } else {
            showProgress();

            Call<KeywordSearchResponse> keywordSearchResponseCall = RestApi.get().getRestService().keywordSearch(keyword);
            keywordSearchResponseCall.enqueue(new Callback<KeywordSearchResponse>() {
                @Override
                public void onResponse(Call<KeywordSearchResponse> call, Response<KeywordSearchResponse> response) {
                    dismissDialog();
                    if (response.isSuccessful()) {
                        if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                            String relatedProducts = new Gson().toJson(response.body().getSearchResult());
                            Intent intent = new Intent(HomeActivity.this, RelatedProductActivity.class);
                            // intent.putExtra("relatedProducts", relatedProducts);
                            sp.edit().putString("relatedProducts", relatedProducts).commit();
                            intent.putExtra("flag", 1);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<KeywordSearchResponse> call, Throwable t) {
                    dismissDialog();
                }
            });
        }
    }

    class HomePageAdapter extends RecyclerView.Adapter<HomeHolder> {
        public Context context;
        public List<DisplayItems> homeDisplayItemsList = new ArrayList<>();
        public List<Products> homeProductsList = new ArrayList<>();

        public HomePageAdapter(Context context) {
            this.context = context;

            setHasStableIds(true);
        }

        public void addItems(List<DisplayItems> homeDisplayItemsList) {
            this.homeDisplayItemsList = homeDisplayItemsList;
            notifyDataSetChanged();
        }

        @Override
        public HomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_home, parent, false);

            return new HomeHolder(itemView);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final HomeHolder holder, int position) {
            List<Banner> banners = new ArrayList<>();

            if (homeDisplayItemsList.get(position).getBanners().size() > 0) {
                for (int i = 0; i < homeDisplayItemsList.get(position).getBanners().size(); i++) {
                    // Log.e("homeListSize::::", i + "");
                    //add banner using image url
                    banners.add(new RemoteBanner(homeDisplayItemsList.get(position).getBanners().get(i).getImage()));
                }
                holder.getBannerSlider().setBanners(banners);
                holder.getBannerSlider().setOnBannerClickListener(new OnBannerClickListener() {
                    @Override
                    public void onClick(int position) {
                        //  Toast.makeText(HomeActivity.this, "Banner with position " + String.valueOf(position) + " clicked!", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                holder.getBannerSlider().setVisibility(View.GONE);
            }
            // Log.d("homeProductSize__", homeDisplayItemsList.get(holder.getAdapterPosition()).getProducts().size()+"");
            if ((homeDisplayItemsList.get(holder.getAdapterPosition()).getProducts().size() > 3)) {
                holder.getLlseeMore().setVisibility(View.VISIBLE);
            } else {
                holder.getLlseeMore().setVisibility(View.GONE);
            }
            holder.getTxtSeeMore().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
              /*      String seeMoreProducts = new Gson().toJson(homeDisplayItemsList.get(holder.getAdapterPosition()).getProducts());
                    sp.edit().putString("seeMoreProducts", seeMoreProducts).commit();
                   Intent intent = new Intent(HomeActivity.this, SeeMoreProducts.class);
                   intent.putExtra("adapterPosition",homeDisplayItemsList.get(holder.getAdapterPosition()).getProducts().toString());
                    startActivity(intent);
                    finish();*/
                }
            });
            HomeProductsAdapter adapter = new HomeProductsAdapter(context);
            holder.getTxtType().setText(homeDisplayItemsList.get(position).getType());
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            holder.getRvProducts().setLayoutManager(layoutManager);
            adapter.addItems(homeDisplayItemsList.get(holder.getAdapterPosition()).getProducts());
            // Log.e("products size::::", homeDisplayItemsList.get(holder.getAdapterPosition()).getProducts().size() + "");
            holder.getRvProducts().setAdapter(adapter);

        }

        @Override
        public int getItemCount() {
            return homeDisplayItemsList.size();
        }
    }

    class HomeProductsAdapter extends RecyclerView.Adapter<HomeProductsHolder> {
        public Context context;
        public List<Products> homeProductsList = new ArrayList<>();
        RecyclerView rvProducts;

        public HomeProductsAdapter(Context context) {
            this.context = context;
            setHasStableIds(true);
        }

        public void addItems(List<Products> homeProductsList) {
            // Log.e("Home products Size:::", homeProductsList.size() + "");
            this.homeProductsList = homeProductsList;
            //notifyDataSetChanged();
        }

        @Override
        public HomeProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_products, parent, false);

            return new HomeProductsHolder(itemView);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(HomeProductsHolder holder, final int position) {
            holder.getTxtProductTitle().setText(homeProductsList.get(position).getTitle() + homeProductsList.get(position).getProduct_id());
            holder.getTxtProductDesc().setText(homeProductsList.get(position).getShortDescription());
            holder.getGetTxtProductMrp().setText(getResources().getString(R.string.Rs) + "  " + homeProductsList.get(position).getMrp());
            Picasso.with(context).load(homeProductsList.get(position).getImages().get(0).getImage())
                    .into(holder.getImProduct());
            holder.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Log.e("Selected pId::", homeProductsList.get(position).getProduct_id());
                    sp.edit().putString("productId", homeProductsList.get(position).getProduct_id()).commit();
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("flag", 0);
                    Log.d("productId__", homeProductsList.get(position).getProduct_id().toString() + ";;" + customerId);
                    startActivity(intent);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return homeProductsList.size();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }*/


   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.login:
                intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.sign_up:
                intent = new Intent(HomeActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/
}
