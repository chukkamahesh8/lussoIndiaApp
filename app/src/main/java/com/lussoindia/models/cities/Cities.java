package com.lussoindia.models.cities;

/**
 * Created by Thriveni on 5/23/2018.
 */

public class Cities {
    private String city_id;

    private String city_name;

    private String shippingCharges;

    private String stateId;

    public String getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (String city_id)
    {
        this.city_id = city_id;
    }

    public String getCity_name ()
    {
        return city_name;
    }

    public void setCity_name (String city_name)
    {
        this.city_name = city_name;
    }

    public String getShippingCharges ()
    {
        return shippingCharges;
    }

    public void setShippingCharges (String shippingCharges)
    {
        this.shippingCharges = shippingCharges;
    }

    public String getStateId ()
    {
        return stateId;
    }

    public void setStateId (String stateId)
    {
        this.stateId = stateId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [city_id = "+city_id+", city_name = "+city_name+", shippingCharges = "+shippingCharges+", stateId = "+stateId+"]";
    }
}
