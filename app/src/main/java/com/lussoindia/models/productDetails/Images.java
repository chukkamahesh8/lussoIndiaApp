package com.lussoindia.models.productDetails;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class Images {
    private String id;

    private String image;

    private String productId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", image = " + image + ", productId = " + productId + "]";
    }
}
