package com.lussoindia.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.lussoindia.R;

import butterknife.BindView;
import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by Thriveni on 5/20/2018.
 */

public class CartHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.txt_product_title)
    TextView txtProductTitle;

    @BindView(R.id.txt_amount)
    TextView txtAmount;

    @BindView(R.id.txt_quantity)
    TextView txtQuantity;

    public LinearLayout getLlAddQty() {
        return llAddQty;
    }

    public void setLlAddQty(LinearLayout llAddQty) {
        this.llAddQty = llAddQty;
    }

    @BindView(R.id.ll_add_qty)
    LinearLayout llAddQty;

    @BindView(R.id.txt_date)
    TextView txtDate;

    @BindView(R.id.txt_total_amount)
    TextView txtTotalAmount;

    @BindView(R.id.im_remove_product)
    ImageView imRemoveProduct;

    public ImageView getImDeleteItem() {
        return imDeleteItem;
    }

    @BindView(R.id.im_delete_item)
    ImageView imDeleteItem;

    @BindView(R.id.im_add_product)
    ImageView imAddProduct;

    public TextView getTxtTotalAmount() {
        return txtTotalAmount;
    }

    public ImageView getImProductImz() {
        return imProductImz;
    }

    @BindView(R.id.im_product)
    ImageView imProductImz;

    public ImageView getImRemoveProduct() {
        return imRemoveProduct;
    }

    public ImageView getImAddProduct() {
        return imAddProduct;
    }

    public TextView getTxtProductTitle() {
        return txtProductTitle;
    }

    public TextView getTxtQuantity() {
        return txtQuantity;
    }

    public TextView getAmount() {
        return txtAmount;
    }

    public TextView getTxtDate() {
        return txtDate;
    }

    public TextView getTxtAmount() {
        return txtTotalAmount;
    }


    public CartHolder(View itemView) {
        super(itemView);
    }
}
