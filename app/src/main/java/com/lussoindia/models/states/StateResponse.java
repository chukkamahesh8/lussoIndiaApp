package com.lussoindia.models.states;

import java.util.List;

/**
 * Created by Thriveni on 5/23/2018.
 */

public class StateResponse {
    private List<States> states;

    private String success;

    public List<States> getStates ()
    {
        return states;
    }

    public void setStates (List<States> states)
    {
        this.states = states;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [states = "+states+", success = "+success+"]";
    }
}
