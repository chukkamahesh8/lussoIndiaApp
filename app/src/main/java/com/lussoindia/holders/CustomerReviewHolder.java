package com.lussoindia.holders;

import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.lussoindia.R;

import butterknife.BindView;

/**
 * Created by Thriveni on 5/8/2018.
 */

public class CustomerReviewHolder extends AbstrctRecyclerViewholder {
    @BindView(R.id.txt_review_name)
    TextView txtReviewName;

    @BindView(R.id.txt_customer_name)
    TextView txtCustomerName;

    @BindView(R.id.txt_city)
    TextView txtCity;

    @BindView(R.id.rating_bar)
    RatingBar ratingBar;

    public TextView getTxtReviewName() {
        return txtReviewName;
    }

    public TextView getTxtCustomerName() {
        return txtCustomerName;
    }

    public TextView getTxtCity() {
        return txtCity;
    }

    public RatingBar getRatingBar() {
        return ratingBar;
    }


    public CustomerReviewHolder(View itemView) {
        super(itemView);
    }
}
