package com.lussoindia.models.prodcutReview;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class ProductReview {
    private String response;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    private String success;

    private List<Reviews> reviews;

    private String reviewCount;

    private String rating;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<Reviews> getReviews() {
        return reviews;
    }

    public void setReviews(List<Reviews> reviews) {
        this.reviews = reviews;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "ClassPojo [response = " + response + ", reviews = " + reviews + ", reviewCount = " + reviewCount + ", rating = " + rating + "]";
    }
}
