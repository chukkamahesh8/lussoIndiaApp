package com.lussoindia.models;

/**
 * Created by Thriveni on 6/6/2018.
 */

public class Items {
    private String total;

    private String igstAmt;

    private String sgstPercent;

    private String cgstAmt;

  //  private String return;

    private String reverseChargePercent;

    private String pid;

    private String qty;

    private String reverseChargeAmt;

    private String sgstAmt;

    private String billid;

    private String id;

    private String rate;

    private String custprodid;

    private String totTaxAmt;

    private String cgstPercent;

    private String dis;

    private String totTaxPercent;

    private String igstPercent;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getIgstAmt ()
    {
        return igstAmt;
    }

    public void setIgstAmt (String igstAmt)
    {
        this.igstAmt = igstAmt;
    }

    public String getSgstPercent ()
    {
        return sgstPercent;
    }

    public void setSgstPercent (String sgstPercent)
    {
        this.sgstPercent = sgstPercent;
    }

    public String getCgstAmt ()
    {
        return cgstAmt;
    }

    public void setCgstAmt (String cgstAmt)
    {
        this.cgstAmt = cgstAmt;
    }

   /* public String getReturn ()
    {
        return return;
    }

    public void setReturn (String return)
    {
        this.return = return;
    }*/

    public String getReverseChargePercent ()
    {
        return reverseChargePercent;
    }

    public void setReverseChargePercent (String reverseChargePercent)
    {
        this.reverseChargePercent = reverseChargePercent;
    }

    public String getPid ()
    {
        return pid;
    }

    public void setPid (String pid)
    {
        this.pid = pid;
    }

    public String getQty ()
    {
        return qty;
    }

    public void setQty (String qty)
    {
        this.qty = qty;
    }

    public String getReverseChargeAmt ()
    {
        return reverseChargeAmt;
    }

    public void setReverseChargeAmt (String reverseChargeAmt)
    {
        this.reverseChargeAmt = reverseChargeAmt;
    }

    public String getSgstAmt ()
    {
        return sgstAmt;
    }

    public void setSgstAmt (String sgstAmt)
    {
        this.sgstAmt = sgstAmt;
    }

    public String getBillid ()
    {
        return billid;
    }

    public void setBillid (String billid)
    {
        this.billid = billid;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getRate ()
    {
        return rate;
    }

    public void setRate (String rate)
    {
        this.rate = rate;
    }

    public String getCustprodid ()
    {
        return custprodid;
    }

    public void setCustprodid (String custprodid)
    {
        this.custprodid = custprodid;
    }

    public String getTotTaxAmt ()
    {
        return totTaxAmt;
    }

    public void setTotTaxAmt (String totTaxAmt)
    {
        this.totTaxAmt = totTaxAmt;
    }

    public String getCgstPercent ()
    {
        return cgstPercent;
    }

    public void setCgstPercent (String cgstPercent)
    {
        this.cgstPercent = cgstPercent;
    }

    public String getDis ()
    {
        return dis;
    }

    public void setDis (String dis)
    {
        this.dis = dis;
    }

    public String getTotTaxPercent ()
    {
        return totTaxPercent;
    }

    public void setTotTaxPercent (String totTaxPercent)
    {
        this.totTaxPercent = totTaxPercent;
    }

    public String getIgstPercent ()
    {
        return igstPercent;
    }

    public void setIgstPercent (String igstPercent)
    {
        this.igstPercent = igstPercent;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [total = "+total+", igstAmt = "+igstAmt+", sgstPercent = "+sgstPercent+", cgstAmt = "+cgstAmt+", return = "+0+", reverseChargePercent = "+reverseChargePercent+", pid = "+pid+", qty = "+qty+", reverseChargeAmt = "+reverseChargeAmt+", sgstAmt = "+sgstAmt+", billid = "+billid+", id = "+id+", rate = "+rate+", custprodid = "+custprodid+", totTaxAmt = "+totTaxAmt+", cgstPercent = "+cgstPercent+", dis = "+dis+", totTaxPercent = "+totTaxPercent+", igstPercent = "+igstPercent+"]";
    }
}
