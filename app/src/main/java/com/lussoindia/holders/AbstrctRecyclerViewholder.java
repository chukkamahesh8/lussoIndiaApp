package com.lussoindia.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class AbstrctRecyclerViewholder extends RecyclerView.ViewHolder {
    private final View itemView;

    public AbstrctRecyclerViewholder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        ButterKnife.bind(this, itemView);

    }

    public View getRootView() {
        return itemView;
    }
}
