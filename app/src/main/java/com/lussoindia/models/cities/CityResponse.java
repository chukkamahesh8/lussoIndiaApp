package com.lussoindia.models.cities;

import java.util.List;

/**
 * Created by Thriveni on 5/23/2018.
 */

public class CityResponse {

    private List<Cities> Cities;

    private String success;

    public List<Cities> getCities ()
    {
        return Cities;
    }

    public void setCities (List<Cities> Cities)
    {
        this.Cities = Cities;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Cities = "+Cities+", success = "+success+"]";
    }

}
