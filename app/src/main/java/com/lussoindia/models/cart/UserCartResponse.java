package com.lussoindia.models.cart;

import java.util.List;

/**
 * Created by Thriveni on 5/20/2018.
 */

public class UserCartResponse {
    private String message;

    private ResponseData responseData;

    private String success;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ResponseData getResponseData ()
    {
        return responseData;
    }

    public void setResponseData (ResponseData responseData)
    {
        this.responseData = responseData;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", responseData = "+responseData+", success = "+success+"]";
    }
}
