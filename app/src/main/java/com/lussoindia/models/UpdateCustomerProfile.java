package com.lussoindia.models;

/**
 * Created by Thriveni on 5/24/2018.
 */

public class UpdateCustomerProfile {
    private String message;

    private String userid;

    private String success;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", userid = "+userid+", success = "+success+"]";
    }
}
