package com.lussoindia.models.getAllCategories;

import java.util.List;

/**
 * Created by Thriveni on 5/17/2018.
 */

public class Categories {
    private String id;

    private String level;

    private String name;

    private List<Children> children;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLevel ()
    {
        return level;
    }

    public void setLevel (String level)
    {
        this.level = level;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public List<Children> getChildren ()
    {
        return children;
    }

    public void setChildren (List<Children> children)
    {
        this.children = children;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", level = "+level+", name = "+name+", children = "+children+"]";
    }
}
