package com.lussoindia.models.getAllCategories;

import java.util.List;

/**
 * Created by Thriveni on 5/17/2018.
 */

public class GetAllCategoriesResponse {
    private List<Categories> categories;

    private String success;

    public List<Categories> getCategories ()
    {
        return categories;
    }

    public void setCategories (List<Categories> categories)
    {
        this.categories = categories;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [categories = "+categories+", success = "+success+"]";
    }
}
