package com.lussoindia.models.updateAddress;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class UpdateAddress {
    private String message;

    private String response;

    private String userid;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", response = " + response + ", userid = " + userid + "]";
    }
}
