package com.lussoindia.models.getCustomerDetailsByCode;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class GetCustomerDetailsByCode {
    private String success;

    private List<CustomerDetails> customerDetails;

    public String getsuccess ()
    {
        return success;
    }

    public void setsuccess (String success)
    {
        this.success = success;
    }

    public List<CustomerDetails> getCustomerDetails ()
    {
        return customerDetails;
    }

    public void setCustomerDetails (List<CustomerDetails> customerDetails)
    {
        this.customerDetails = customerDetails;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [success = "+success+", customerDetails = "+customerDetails+"]";
    }
}
