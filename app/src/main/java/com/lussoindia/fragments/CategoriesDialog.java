package com.lussoindia.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lussoindia.R;
import com.lussoindia.activities.HomeActivity;
import com.lussoindia.activities.RelatedProductActivity;
import com.lussoindia.models.EmptyRequest;
import com.lussoindia.models.categorySearch.CategorySearchResponse;
import com.lussoindia.models.getAllCategories.Categories;
import com.lussoindia.models.getAllCategories.Children;
import com.lussoindia.models.getAllCategories.GetAllCategoriesResponse;
import com.lussoindia.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 5/17/2018.
 */

public class CategoriesDialog extends DialogFragment {
    @BindView(R.id.exp_list)
    ExpandableListView expandableListView;

    @BindView(R.id.txt_plz_wait)
    TextView txtPlzWait;

    private String customerId = "", categoryId = "";
    private SharedPreferences sp = null;
    private ProgressDialog pDialog = null;
    ExpandableListAdapter ExpAdapter;
    @BindView(R.id.im_close)
    ImageView imClose;
    int selectedGroupId = 0, previousGroup = -1;
    ;

    public static CategoriesDialog newInstance(String title) {

        CategoriesDialog frag = new CategoriesDialog();
        /*Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);*/
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.dialog_fragment_categories, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        sp = getActivity().getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId = sp.getString("customerId", "");

        // getAllCategories();
        Call<GetAllCategoriesResponse> getCategories = RestApi.get().getRestService().getAllCategories();
        getCategories.enqueue(new Callback<GetAllCategoriesResponse>() {
            @Override
            public void onResponse(Call<GetAllCategoriesResponse> call, Response<GetAllCategoriesResponse> response) {
                if (response.body().getSuccess().equalsIgnoreCase("true")) {
                    txtPlzWait.setVisibility(View.GONE);
                    expandableListView.setVisibility(View.VISIBLE);
                    //  if ("true".equalsIgnoreCase(response.body().getResponse())) {
                    // Log.d("categories_",response.body().getCategories().toString());
                    // Log.d("children___",response.body().getCategories().get(0).getChildren().toString());
                    List<Categories> selectedMealObject = response.body().getCategories();
                    ExpAdapter = new ExpandableListAdapter(getActivity(),
                            (ArrayList<Categories>) selectedMealObject);
                    expandableListView.setAdapter(ExpAdapter);
                    // }
                } else {
                    // Log.d("faild",response.body().getSuccess().toString());
                    txtPlzWait.setVisibility(View.VISIBLE);
                    txtPlzWait.setText("No Categories Found");
                    expandableListView.setVisibility(View.GONE);
                    //

                }
                if (pDialog != null) {
                    pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetAllCategoriesResponse> call, Throwable t) {
                //  Log.d("faild",t.getMessage().toString());
                if (pDialog != null) {
                    pDialog.dismiss();
                }
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        imClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    private void getAllCategories() {
        // Call<GetAllCategoriesResponse> getAllCategoriesResponseCall= RestApi.get().getRestService().getAllCategories(EmptyRequest.INSTANCE);
        Call<GetAllCategoriesResponse> getAllCategoriesResponseCall = RestApi.get().getRestService().getAllCategories();
        getAllCategoriesResponseCall.enqueue(new Callback<GetAllCategoriesResponse>() {
            @Override
            public void onResponse(Call<GetAllCategoriesResponse> call, Response<GetAllCategoriesResponse> response) {
                // Log.d("categories_",response.body().getSuccess().toString());
                if (response.body().getSuccess().equalsIgnoreCase("true")) {
                    //  if ("true".equalsIgnoreCase(response.body().getResponse())) {
                    //  Log.d("categories_",response.body().getCategories().toString());
                    // Log.d("children___",response.body().getCategories().get(0).getChildren().toString());
                    List<Categories> selectedMealObject = response.body().getCategories();
                    ExpAdapter = new ExpandableListAdapter(getActivity(),
                            (ArrayList<Categories>) selectedMealObject);
                    expandableListView.setAdapter(ExpAdapter);
                    // }
                } else {
                    //  Log.d("faild",response.body().getSuccess().toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllCategoriesResponse> call, Throwable t) {
                //  Log.d("faild",t.getMessage().toString());
            }
        });
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {
        private Context context;
        private ArrayList<Categories> groups;

        public ExpandableListAdapter(Context context, ArrayList<Categories> groups) {
            this.context = context;
            this.groups = groups;

        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            List<Children> chList = groups.get(groupPosition).getChildren();
            return chList.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(final int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final Children child = (Children) getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) context
                        .getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.item_children, null);
            }

            final TextView tv = (TextView) convertView.findViewById(R.id.txt_service_name);
            if (!"".equalsIgnoreCase(child.getName())) {
                tv.setText(child.getName().toString());
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryId = child.getId();
                    categorySearch(customerId, categoryId);
                }
            });

            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            ArrayList<Children> chList = (ArrayList<Children>) groups.get(groupPosition).getChildren();
            return chList.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groups.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groups.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            final Categories group = (Categories) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) context
                        .getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = inf.inflate(R.layout.item_category_list, null);
            }
            TextView tv = (TextView) convertView.findViewById(R.id.list_item_genre_name);
            tv.setText(group.getName());

            ImageView imageView = (ImageView) convertView.findViewById(R.id.list_item_genre_arrow);
            if (group.getChildren() != null) {
                if (group.getChildren().size() > 0) {

                } else {
                    imageView.setVisibility(View.GONE);
                }
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getChildrenCount(groupPosition) > 0) {
                       /* if (selectedGroupId != groupPosition) {
                            selectedGroupId = groupPosition;
                            expandableListView.expandGroup(groupPosition);
                        } else {
                            expandableListView.collapseGroup(groupPosition);
                        }*/
                        expandableListView.expandGroup(groupPosition);
                    } else {
                        categorySearch(customerId, group.getId());
                    }

                }
            });
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }

    private void categorySearch(String customerId, String categoryId) {
        pDialog = ProgressDialog.show(getActivity(), "", "", false);
        Call<CategorySearchResponse> categorySearchResponseCall = RestApi.get().getRestService().categorySearch(customerId,
                categoryId);
        categorySearchResponseCall.enqueue(new Callback<CategorySearchResponse>() {
            @Override
            public void onResponse(Call<CategorySearchResponse> call, Response<CategorySearchResponse> response) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        String relatedProducts = new Gson().toJson(response.body().getCategoryProducts());
                        Intent intent = new Intent(getActivity(), RelatedProductActivity.class);
                        // intent.putExtra("relatedProducts", relatedProducts);
                        sp.edit().putString("relatedProducts", relatedProducts).commit();
                        intent.putExtra("flag", 2);
                        startActivity(intent);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CategorySearchResponse> call, Throwable t) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
            }
        });
    }
}
