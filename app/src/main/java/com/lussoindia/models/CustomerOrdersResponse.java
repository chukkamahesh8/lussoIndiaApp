package com.lussoindia.models;

/**
 * Created by Thriveni on 6/6/2018.
 */

public class CustomerOrdersResponse {
    private String taxtotal;

    private String paymentby;

    private String reverseChargePercent;

    private String billcatid;

    private String customerPhone;

    private String sgst;

    private String btadr;

    private String btpno;

    private String deletedon;

    private String billnumber;

    private String id;

    private String companyid;

    private String billdate;

    private String billTypeId;

    private String details;

    private String creditdays;

    private String taxtype;

    private String btnm;

    private String customerName;

    private String sgstPercent;

    private String status;

    private String custgstno;

    private String invoiceType;

    private String billtotal;

    private String reverseCharge;

    private String orderStatus;

    private String vehicleno;

    private String statusupdateddate;

    private String customerid;

    private String cgstPercent;

    private String stateId;

    private String igst;

    private String customerAddress;

    private String remindindays;

    private String pm;

    private String grosstotal;

    private String cgst;

    private String igstPercent;

    public String getTaxtotal ()
    {
        return taxtotal;
    }

    public void setTaxtotal (String taxtotal)
    {
        this.taxtotal = taxtotal;
    }

    public String getPaymentby ()
    {
        return paymentby;
    }

    public void setPaymentby (String paymentby)
    {
        this.paymentby = paymentby;
    }

    public String getReverseChargePercent ()
    {
        return reverseChargePercent;
    }

    public void setReverseChargePercent (String reverseChargePercent)
    {
        this.reverseChargePercent = reverseChargePercent;
    }

    public String getBillcatid ()
    {
        return billcatid;
    }

    public void setBillcatid (String billcatid)
    {
        this.billcatid = billcatid;
    }

    public String getCustomerPhone ()
    {
        return customerPhone;
    }

    public void setCustomerPhone (String customerPhone)
    {
        this.customerPhone = customerPhone;
    }

    public String getSgst ()
    {
        return sgst;
    }

    public void setSgst (String sgst)
    {
        this.sgst = sgst;
    }

    public String getBtadr ()
    {
        return btadr;
    }

    public void setBtadr (String btadr)
    {
        this.btadr = btadr;
    }

    public String getBtpno ()
    {
        return btpno;
    }

    public void setBtpno (String btpno)
    {
        this.btpno = btpno;
    }

    public String getDeletedon ()
    {
        return deletedon;
    }

    public void setDeletedon (String deletedon)
    {
        this.deletedon = deletedon;
    }

    public String getBillnumber ()
    {
        return billnumber;
    }

    public void setBillnumber (String billnumber)
    {
        this.billnumber = billnumber;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCompanyid ()
    {
        return companyid;
    }

    public void setCompanyid (String companyid)
    {
        this.companyid = companyid;
    }

    public String getBilldate ()
    {
        return billdate;
    }

    public void setBilldate (String billdate)
    {
        this.billdate = billdate;
    }

    public String getBillTypeId ()
    {
        return billTypeId;
    }

    public void setBillTypeId (String billTypeId)
    {
        this.billTypeId = billTypeId;
    }

    public String getDetails ()
    {
        return details;
    }

    public void setDetails (String details)
    {
        this.details = details;
    }

    public String getCreditdays ()
    {
        return creditdays;
    }

    public void setCreditdays (String creditdays)
    {
        this.creditdays = creditdays;
    }

    public String getTaxtype ()
    {
        return taxtype;
    }

    public void setTaxtype (String taxtype)
    {
        this.taxtype = taxtype;
    }

    public String getBtnm ()
    {
        return btnm;
    }

    public void setBtnm (String btnm)
    {
        this.btnm = btnm;
    }

    public String getCustomerName ()
    {
        return customerName;
    }

    public void setCustomerName (String customerName)
    {
        this.customerName = customerName;
    }

    public String getSgstPercent ()
    {
        return sgstPercent;
    }

    public void setSgstPercent (String sgstPercent)
    {
        this.sgstPercent = sgstPercent;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getCustgstno ()
    {
        return custgstno;
    }

    public void setCustgstno (String custgstno)
    {
        this.custgstno = custgstno;
    }

    public String getInvoiceType ()
    {
        return invoiceType;
    }

    public void setInvoiceType (String invoiceType)
    {
        this.invoiceType = invoiceType;
    }

    public String getBilltotal ()
    {
        return billtotal;
    }

    public void setBilltotal (String billtotal)
    {
        this.billtotal = billtotal;
    }

    public String getReverseCharge ()
    {
        return reverseCharge;
    }

    public void setReverseCharge (String reverseCharge)
    {
        this.reverseCharge = reverseCharge;
    }

    public String getOrderStatus ()
    {
        return orderStatus;
    }

    public void setOrderStatus (String orderStatus)
    {
        this.orderStatus = orderStatus;
    }

    public String getVehicleno ()
    {
        return vehicleno;
    }

    public void setVehicleno (String vehicleno)
    {
        this.vehicleno = vehicleno;
    }

    public String getStatusupdateddate ()
    {
        return statusupdateddate;
    }

    public void setStatusupdateddate (String statusupdateddate)
    {
        this.statusupdateddate = statusupdateddate;
    }

    public String getCustomerid ()
    {
        return customerid;
    }

    public void setCustomerid (String customerid)
    {
        this.customerid = customerid;
    }

    public String getCgstPercent ()
    {
        return cgstPercent;
    }

    public void setCgstPercent (String cgstPercent)
    {
        this.cgstPercent = cgstPercent;
    }

    public String getStateId ()
    {
        return stateId;
    }

    public void setStateId (String stateId)
    {
        this.stateId = stateId;
    }

    public String getIgst ()
    {
        return igst;
    }

    public void setIgst (String igst)
    {
        this.igst = igst;
    }

    public String getCustomerAddress ()
    {
        return customerAddress;
    }

    public void setCustomerAddress (String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getRemindindays ()
    {
        return remindindays;
    }

    public void setRemindindays (String remindindays)
    {
        this.remindindays = remindindays;
    }

    public String getPm ()
    {
        return pm;
    }

    public void setPm (String pm)
    {
        this.pm = pm;
    }

    public String getGrosstotal ()
    {
        return grosstotal;
    }

    public void setGrosstotal (String grosstotal)
    {
        this.grosstotal = grosstotal;
    }

    public String getCgst ()
    {
        return cgst;
    }

    public void setCgst (String cgst)
    {
        this.cgst = cgst;
    }

    public String getIgstPercent ()
    {
        return igstPercent;
    }

    public void setIgstPercent (String igstPercent)
    {
        this.igstPercent = igstPercent;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [taxtotal = "+taxtotal+", paymentby = "+paymentby+", reverseChargePercent = "+reverseChargePercent+", billcatid = "+billcatid+", customerPhone = "+customerPhone+", sgst = "+sgst+", btadr = "+btadr+", btpno = "+btpno+", deletedon = "+deletedon+", billnumber = "+billnumber+", id = "+id+", companyid = "+companyid+", billdate = "+billdate+", billTypeId = "+billTypeId+", details = "+details+", creditdays = "+creditdays+", taxtype = "+taxtype+", btnm = "+btnm+", customerName = "+customerName+", sgstPercent = "+sgstPercent+", status = "+status+", custgstno = "+custgstno+", invoiceType = "+invoiceType+", billtotal = "+billtotal+", reverseCharge = "+reverseCharge+", orderStatus = "+orderStatus+", vehicleno = "+vehicleno+", statusupdateddate = "+statusupdateddate+", customerid = "+customerid+", cgstPercent = "+cgstPercent+", stateId = "+stateId+", igst = "+igst+", customerAddress = "+customerAddress+", remindindays = "+remindindays+", pm = "+pm+", grosstotal = "+grosstotal+", cgst = "+cgst+", igstPercent = "+igstPercent+"]";
    }
}
