package com.lussoindia.models.cart;

import java.util.List;

/**
 * Created by Thriveni on 5/20/2018.
 */

public class ResponseData {
    private List<CartItems> cartItems;

    private CustomerDetails customerDetails;

    private Totals totals;

    public List<CartItems> getCartItems ()
    {
        return cartItems;
    }

    public void setCartItems (List<CartItems> cartItems)
    {
        this.cartItems = cartItems;
    }

    public CustomerDetails getCustomerDetails ()
    {
        return customerDetails;
    }

    public void setCustomerDetails (CustomerDetails customerDetails)
    {
        this.customerDetails = customerDetails;
    }

    public Totals getTotals ()
    {
        return totals;
    }

    public void setTotals (Totals totals)
    {
        this.totals = totals;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cartItems = "+cartItems+", customerDetails = "+customerDetails+", totals = "+totals+"]";
    }
}
