package com.lussoindia.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lussoindia.R;
import com.lussoindia.adapters.*;

import butterknife.BindView;

/**
 * Created by Thriveni on 6/13/2018.
 */

public class Home1ProductHolder extends com.lussoindia.adapters.AbstractViewHolder {
    public Home1ProductHolder(View view) {
        super(view);
    }

    @BindView(R.id.im_product)
    ImageView imProduct;

    @BindView(R.id.txt_product_title)
    TextView txtProductTitle;

    @BindView(R.id.txt_product_desc)
    TextView txtProductDesc;

    @BindView(R.id.txt_product_mrp)
    TextView getTxtProductMrp;

    public ImageView getImProduct() {
        return imProduct;
    }

    public TextView getTxtProductTitle() {
        return txtProductTitle;
    }

    public TextView getTxtProductDesc() {
        return txtProductDesc;
    }

    public TextView getGetTxtProductMrp() {
        return getTxtProductMrp;
    }
}
