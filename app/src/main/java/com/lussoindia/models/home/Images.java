package com.lussoindia.models.home;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class Images {
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ClassPojo [image = " + image + "]";
    }
}
