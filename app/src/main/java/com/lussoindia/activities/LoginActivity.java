package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.models.getCustomerDetailsByCode.GetCustomerDetailsByCode;
import com.lussoindia.models.home.HomeResponse;
import com.lussoindia.models.login.LoginResponse;
import com.lussoindia.models.register.RegisterResponse;
import com.lussoindia.network.RestApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.xm.weidongjian.popuphelper.PopupWindowHelper;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class LoginActivity extends BaseActivity {
    @BindView(R.id.et_username)
    EditText etUserName;

    @BindView(R.id.et_password1)
    EditText etPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.ll_login)
    LinearLayout llLogin;

    @BindView(R.id.ll_sign_up)
    LinearLayout llSignUp;


    @BindView(R.id.et_Customercode)
    EditText etCustomerCode;

    @BindView(R.id.btn_load_c_details)
    ImageView btnCDetails;

    @BindView(R.id.et_UserName)
    EditText etUserName1;

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_password)
    EditText etPassword1;

    @BindView(R.id.et_confirm_pwd)
    EditText etConfirmPwd;

    @BindView(R.id.btn_sign_up)
    Button btnRegister;

    @BindView(R.id.segmented2)
    SegmentedGroup segmented2;

    @BindView(R.id.rb_login)
    RadioButton rb_login;

    @BindView(R.id.rb_sign_up)
    RadioButton rb_sign_up;

    private SharedPreferences sp;
    Toolbar toolbar;
    TextView txtPageTitle;
    ImageView imAppLogo;

    private PopupWindowHelper popupWindowHelper;
    private View popView;
    private String customerId = "";
    private int flag = 0;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            }
        });
        txtTitle.setText("Login");
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        btnCDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                flag = 1;
                if (TextUtils.isEmpty(etCustomerCode.getText().toString().trim())) {
                    etCustomerCode.setError("Please enter Customer Code");
                    etCustomerCode.requestFocus();
                } else {
                    showProgress();
                    String customerCode = etCustomerCode.getText().toString();
                    customerId = customerCode;
                    Call<GetCustomerDetailsByCode> getCustomerDetailsResponseCall = RestApi.get().getRestService().getCustomerDetailsByCode(customerCode);
                    getCustomerDetailsResponseCall.enqueue(new Callback<GetCustomerDetailsByCode>() {
                        @Override
                        public void onResponse(Call<GetCustomerDetailsByCode> call, Response<GetCustomerDetailsByCode> response) {
                            dismissDialog();
                            if (response.isSuccessful()) {
                                if ("true".equalsIgnoreCase(response.body().getsuccess())) {
                                    com.lussoindia.models.getCustomerDetailsByCode.CustomerDetails obj = response.body().getCustomerDetails().get(0);
                                    if (!TextUtils.isEmpty(obj.getUsername())) {
                                        etUserName.setText(obj.getUsername());
                                    }
                                    if (!TextUtils.isEmpty(obj.getName())) {
                                        etName.setText(obj.getName());
                                    }
                                    if (!TextUtils.isEmpty(obj.getPassword())) {
                                        etPassword.setText(obj.getPassword());
                                    }
                                    if (!TextUtils.isEmpty(obj.getEmail())) {
                                        etEmail.setText(obj.getEmail());
                                    }
                                    if (!TextUtils.isEmpty(obj.getPhone_no())) {
                                        etPhone.setText(obj.getPhone_no());
                                    }
                                } else {
                                    dismissDialog();
                                    Toast.makeText(LoginActivity.this, response.message().toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<GetCustomerDetailsByCode> call, Throwable t) {
                            dismissDialog();
                        }
                    });
                }
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();

            }
        });
        segmented2.setTintColor(Color.parseColor("#FFFFFF"), Color.parseColor("#2D6096"));
        rb_login.setChecked(false);

        segmented2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_login:
                        llLogin.setVisibility(View.VISIBLE);
                        llSignUp.setVisibility(View.GONE);
                        break;
                    case R.id.rb_sign_up:
                        llLogin.setVisibility(View.GONE);
                        llSignUp.setVisibility(View.VISIBLE);
                        break;


                    default:
                        break;
                }
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                register();
            }
        });


       /* etUserName1.setText("test");
        etName.setText("test");
        etEmail.setText("test@gmail.com");
        etPhone.setText("test");
        etPassword1.setText("test");
        etConfirmPwd.setText("test");*/
    }

    private void register() {
        if (TextUtils.isEmpty(etUserName1.getText().toString().trim())) {
            etUserName1.setError("Please enter UserName");
            etUserName1.requestFocus();
        } else if (TextUtils.isEmpty(etName.getText().toString().trim())) {
            etName.setError("Please enter Name");
            etName.requestFocus();
        } else if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            etEmail.setError("Please enter Email");
            etEmail.requestFocus();
        } else if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
            etPhone.setError("Please enter Mobile number");
            etPhone.requestFocus();
        } else if (TextUtils.isEmpty(etPassword1.getText().toString().trim())) {
            etPassword1.setError("Please enter Password");
            etPassword1.requestFocus();
        } else if (TextUtils.isEmpty(etConfirmPwd.getText().toString().trim())) {
            etConfirmPwd.setError("Please enter Confirm Password");
            etConfirmPwd.requestFocus();
        } else if (!TextUtils.equals(etPassword1.getText().toString().trim(), etConfirmPwd.getText().toString().trim())) {
            etConfirmPwd.setError("Confirm password should equal to password");
            etConfirmPwd.requestFocus();
        } else {
            String username = etUserName1.getText().toString();
            String name = etName.getText().toString();
            String email = etEmail.getText().toString();
            String phone = etPhone.getText().toString();
            String password = etPassword1.getText().toString();
            Log.e("customerId", customerId);
            showProgress();
            Call<RegisterResponse> registerResponseCall = RestApi.get().getRestService().registration(username,
                    password,
                    name,
                    email,
                    phone, customerId);

            registerResponseCall.enqueue(
                    new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            dismissDialog();
                            if (response.isSuccessful()) {
                                if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                                    sp.edit().putBoolean("loginStatus", true).commit();
                                    if (flag == 1) {
                                        sp.edit().putString("customerId", response.body().getUserid()).commit();
                                    } else {
                                        sp.edit().putString("customerId", response.body().getCustomerId()).commit();
                                    }
                                    //callHomeservice(response.body().getUserid());
                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            dismissDialog();
                        }
                    }
            );
        }
    }

    private void callHomeservice(String userid) {
        // Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeList();
        Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeListByUserId(userid);
        getHomeList.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                dismissDialog();
            }
        });


    }

    private void login() {
        if (TextUtils.isEmpty(etUserName.getText().toString())) {
            etUserName.setError("username not empty");
            etUserName.requestFocus();
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            etPassword.setError("password not empty");
            etPassword.requestFocus();
        } else {
            String userName = etUserName.getText().toString().trim();
            String password = etPassword.getText().toString().trim();
            //Log.d("loginDetails__",userName+";;;"+password);
            showProgress();
            Call<LoginResponse> loginResponseCall = RestApi.get().getRestService().login(userName, password);
            loginResponseCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    dismissDialog();
                    if (response.isSuccessful()) {
                        if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                            sp.edit().putString("customerId", response.body().getCustomerId())
                                    .putBoolean("loginStatus", true).commit();
                            Log.d("customerId__", response.body().getCustomerId());
                            //callHomeservice(response.body().getUserid());
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    dismissDialog();
                }
            });
        }
    }
/*

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:

                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);

        }
        return (super.onOptionsItemSelected(menuItem));
    }*/

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }
}
