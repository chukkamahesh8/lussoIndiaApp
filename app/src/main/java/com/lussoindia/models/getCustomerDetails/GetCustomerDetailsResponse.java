package com.lussoindia.models.getCustomerDetails;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class GetCustomerDetailsResponse {

    private List<CustomerDetails> customerDetails;

    private String success;

    public List<CustomerDetails> getCustomerDetails ()
    {
        return customerDetails;
    }

    public void setCustomerDetails (List<CustomerDetails> customerDetails)
    {
        this.customerDetails = customerDetails;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [customerDetails = "+customerDetails+", success = "+success+"]";
    }
}
