package com.lussoindia.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lussoindia.R;
import com.lussoindia.adapters.SlidingImage_Adapter;
import com.lussoindia.models.productDetails.Images;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 7/10/2018.
 */

public class ProfilePhotosDialogFragment extends android.support.v4.app.DialogFragment {

    @BindView(R.id.im_cancel)
    ImageView imCancel;

    @BindView(R.id.pager)
    ViewPager mPager;

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private List<Images> userImagesList = new ArrayList<>();
    private int currentUserPosition = 0;


    public static ProfilePhotosDialogFragment newInstance(String title) {
        ProfilePhotosDialogFragment frag = new ProfilePhotosDialogFragment();
        /*Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);*/
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
           // Log.e("args", getArguments().getString("userImages"));
            Type type = new TypeToken<List<Images>>() {
            }.getType();
            List<Images> userImages = new Gson().fromJson(getArguments().getString("userImages"), type);
           // Log.e("images", "user images size" + userImages.size());
            userImagesList = userImages;
            //  currentUserPosition = getArguments().getInt("currentUserPosition");
            if (userImagesList.size() > 0) {
                for (int i = 0; i < userImagesList.size(); i++) {
                    if (userImagesList.get(i).getId().equalsIgnoreCase(getArguments().getString("currentUserPosition"))) {
                        currentUserPosition = i;
                    }
                }
            }
        }

        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_photos, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mPager.setAdapter(new SlidingImage_Adapter(getActivity(), (ArrayList<Images>) userImagesList));
        mPager.setCurrentItem(currentUserPosition);

        imCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }
}

