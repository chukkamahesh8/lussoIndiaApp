package com.lussoindia.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lussoindia.R;

import butterknife.BindView;

/**
 * Created by Thriveni on 6/6/2018.
 */

public class Myordersholder extends AbstrctRecyclerViewholder {

    @BindView(R.id.txt_bill_no)
    TextView txtBillNo;

    @BindView(R.id.txt_myorder_txn_no)
    TextView txtTxnNo;

    @BindView(R.id.txt_myorder_bill_date)
    TextView txtBillDate;

    @BindView(R.id.txt_myorder_bill_total)
    TextView txtBillTotal;

    @BindView(R.id.txt_myorder_tax_total)
    TextView txtTaxTotal;

    @BindView(R.id.txt_myorder_customer)
    TextView txtCustomer;

    @BindView(R.id.txt_myorder_gst_no)
    TextView txtGstNo;

    @BindView(R.id.im_order_details)
    ImageView imOrderDetails;

    public TextView getTxtBillNo() {
        return txtBillNo;
    }

    public TextView getTxtTxnNo() {
        return txtTxnNo;
    }

    public TextView getTxtBillDate() {
        return txtBillDate;
    }

    public TextView getTxtBillTotal() {
        return txtBillTotal;
    }

    public TextView getTxtTaxTotal() {
        return txtTaxTotal;
    }

    public TextView getTxtCustomer() {
        return txtCustomer;
    }

    public TextView getTxtGstNo() {
        return txtGstNo;
    }

    public ImageView getImOrderDetails() {
        return imOrderDetails;
    }


    public Myordersholder(View itemView) {
        super(itemView);
    }
}
