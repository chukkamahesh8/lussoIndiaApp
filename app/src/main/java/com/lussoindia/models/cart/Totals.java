package com.lussoindia.models.cart;

public class Totals {

    private String grossTotal;

    private String totalBeforeTax;

    private String totalSgstAmt;

    private String totalDiscount;

    private String shippingCharges;

    private String totalIgstPercent;

    private String totalSgstPercent;

    private String totalCgstAmt;

    private String totalReverseChargePercent;

    private String totalCgstPercent;

    private String totalReverseChargeAmt;

    private String totalIgstAmt;

    public String getGrossTotal ()
    {
        return grossTotal;
    }

    public void setGrossTotal (String grossTotal)
    {
        this.grossTotal = grossTotal;
    }

    public String getTotalBeforeTax ()
    {
        return totalBeforeTax;
    }

    public void setTotalBeforeTax (String totalBeforeTax)
    {
        this.totalBeforeTax = totalBeforeTax;
    }

    public String getTotalSgstAmt ()
    {
        return totalSgstAmt;
    }

    public void setTotalSgstAmt (String totalSgstAmt)
    {
        this.totalSgstAmt = totalSgstAmt;
    }

    public String getTotalDiscount ()
    {
        return totalDiscount;
    }

    public void setTotalDiscount (String totalDiscount)
    {
        this.totalDiscount = totalDiscount;
    }

    public String getShippingCharges ()
    {
        return shippingCharges;
    }

    public void setShippingCharges (String shippingCharges)
    {
        this.shippingCharges = shippingCharges;
    }

    public String getTotalIgstPercent ()
    {
        return totalIgstPercent;
    }

    public void setTotalIgstPercent (String totalIgstPercent)
    {
        this.totalIgstPercent = totalIgstPercent;
    }

    public String getTotalSgstPercent ()
    {
        return totalSgstPercent;
    }

    public void setTotalSgstPercent (String totalSgstPercent)
    {
        this.totalSgstPercent = totalSgstPercent;
    }

    public String getTotalCgstAmt ()
    {
        return totalCgstAmt;
    }

    public void setTotalCgstAmt (String totalCgstAmt)
    {
        this.totalCgstAmt = totalCgstAmt;
    }

    public String getTotalReverseChargePercent ()
    {
        return totalReverseChargePercent;
    }

    public void setTotalReverseChargePercent (String totalReverseChargePercent)
    {
        this.totalReverseChargePercent = totalReverseChargePercent;
    }

    public String getTotalCgstPercent ()
    {
        return totalCgstPercent;
    }

    public void setTotalCgstPercent (String totalCgstPercent)
    {
        this.totalCgstPercent = totalCgstPercent;
    }

    public String getTotalReverseChargeAmt ()
    {
        return totalReverseChargeAmt;
    }

    public void setTotalReverseChargeAmt (String totalReverseChargeAmt)
    {
        this.totalReverseChargeAmt = totalReverseChargeAmt;
    }

    public String getTotalIgstAmt ()
    {
        return totalIgstAmt;
    }

    public void setTotalIgstAmt (String totalIgstAmt)
    {
        this.totalIgstAmt = totalIgstAmt;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [grossTotal = "+grossTotal+", totalBeforeTax = "+totalBeforeTax+", totalSgstAmt = "+totalSgstAmt+", totalDiscount = "+totalDiscount+", shippingCharges = "+shippingCharges+", totalIgstPercent = "+totalIgstPercent+", totalSgstPercent = "+totalSgstPercent+", totalCgstAmt = "+totalCgstAmt+", totalReverseChargePercent = "+totalReverseChargePercent+", totalCgstPercent = "+totalCgstPercent+", totalReverseChargeAmt = "+totalReverseChargeAmt+", totalIgstAmt = "+totalIgstAmt+"]";
    }
}
