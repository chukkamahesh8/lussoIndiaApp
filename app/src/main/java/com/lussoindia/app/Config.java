package com.lussoindia.app;

/**
 * Created by Rushi on 12/1/2016.
 */

public class Config {

    public static final String TOPIC_GLOBAL = "global";
    public static final String REGISTRATION_COMPLETE = "REGISTRATIONCOMPLETE";
    public static final String PUSH_NOTIFICATION = "PUSH";
    public static final String USER_PROFILE = "PROFILE";
    public static final String NOTIFICATION_MESSAGE = "MESSAGE";
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "AH_FIREBASE";
}
