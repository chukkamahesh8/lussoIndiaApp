package com.lussoindia.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.holders.CartHolder;
import com.lussoindia.models.AddOrderResponse;
import com.lussoindia.models.SucessReponse;
import com.lussoindia.models.cart.CartItems;
import com.lussoindia.models.cart.CustomerDetails;
import com.lussoindia.models.cart.DeleteCartItemResponse;
import com.lussoindia.models.cart.DeleteCartResponse;
import com.lussoindia.models.cart.Totals;
import com.lussoindia.models.cart.UserCartResponse;
import com.lussoindia.models.cart.addCart.AddCartResponse;
import com.lussoindia.network.RestApi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 5/20/2018.
 */

public class CartActivity extends BaseActivity {
    @BindView(R.id.rv_cart)
    RecyclerView rvCart;
    String password;
    @BindView(R.id.im_place_order1)
    ImageView imPlaceorder;
    @BindView(R.id.im_send_quote)
    ImageView imSendQuote;
    @BindView(R.id.txt_empty_cart)
    TextView txtEmpty;
    @BindView(R.id.txt_total_discount)
    TextView txtTotalDiscount;
    @BindView(R.id.txt_total_amount)
    TextView txtTotalAmount;
    @BindView(R.id.rl_below)
    RelativeLayout riBottrom;
    CartAdapter adapter;
    private String customerId = null;
    String productId = "";
    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;
    String flag = "";
    ImageView imMenu, imAppLogo, imCart;
    SharedPreferences sp;
   /* @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;
*/
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
       /* imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(CartActivity.this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            }
        });

        txtTitle.setText("My Cart Details");*/

        toolbar = findViewById(R.id.toolbar);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setVisibility(View.GONE);
        ImageView imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.GONE);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("My Cart Details");
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo);
        imCart = (ImageView) toolbar.findViewById(R.id.im_cart);
        imAppLogo.setVisibility(View.GONE);
        imCart.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        toolbar.getNavigationIcon().setColorFilter(ContextCompat.getColor(CartActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId = sp.getString("customerId", "");// "670";//
        if (getIntent() != null) {
            flag = getIntent().getStringExtra("flag");
            //Log.e("flag", flag);
        }
        getCartItems();
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCart.setLayoutManager(layoutManager);
        adapter = new CartAdapter(this);

        imCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCart();
            }
        });

        imSendQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog();
            }
        });

        imPlaceorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Log.e("addOrder__", "BTN CLICKED");
                showProgress();
                Call<AddOrderResponse> userCartResponseCall = RestApi.get().getRestService().addOrder(customerId);
                userCartResponseCall.enqueue(new Callback<AddOrderResponse>() {
                    @Override
                    public void onResponse(Call<AddOrderResponse> call, Response<AddOrderResponse> response) {
                        dismissDialog();
                        if (response.isSuccessful()) {
                            if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                                // Log.e("addOrder__", response.body().getMessage());
                                //deleteCart();
                                Toast.makeText(CartActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(CartActivity.this, PlaceOrderActivity.class);
                                startActivity(intent);
                                finish();
                            } else {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddOrderResponse> call, Throwable t) {
                        dismissDialog();
                    }
                });
            }
        });

    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CartActivity.this);
        alertDialog.setTitle("Send Quote");
        // alertDialog.setMessage("Enter Password");

        final EditText input = new EditText(CartActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        //alertDialog.setIcon(R.drawable.key);

        alertDialog.setPositiveButton("Send",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        password = input.getText().toString();

                        if (TextUtils.isEmpty(password)) {
                            Toast.makeText(CartActivity.this, "Please enter message", Toast.LENGTH_SHORT).show();
                            input.setError("please enter message");
                            input.requestFocus();
                            //  Log.e("password", password);
                            // sendQuote();
                        } else {
                            // Log.e("password", password);
                            sendQuote();
                        }
                    }
                });

       /* alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });*/

        alertDialog.show();

    }

    private void sendQuote() {
        String customerId = "0";

        if (sp.getBoolean("loginStatus", false)) {
            customerId = sp.getString("customerId", "");
        } else {
            customerId = "0";
        }
        showProgress();
        Call<SucessReponse> sendQuote = RestApi.get().getRestService().sendQuote(customerId, productId, password);
        sendQuote.enqueue(new Callback<SucessReponse>() {
            @Override
            public void onResponse(Call<SucessReponse> call, Response<SucessReponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getResponse())) {
                       /* new AlertDialog.Builder(CartActivity.this)
                                .setTitle("Ecom")
                                .setMessage(
                                        response.body().getMessage())
                                .setIcon(
                                        getResources().getDrawable(
                                                android.R.drawable.ic_dialog_alert))
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });*/
                    }
                }
            }

            @Override
            public void onFailure(Call<SucessReponse> call, Throwable t) {

            }
        });
    }

    private void getCartItems() {
        showProgress();
        Call<UserCartResponse> userCartResponseCall = RestApi.get().getRestService().getUserCart(customerId);
        userCartResponseCall.enqueue(new Callback<UserCartResponse>() {
            @Override
            public void onResponse(Call<UserCartResponse> call, Response<UserCartResponse> response) {
                dismissDialog();
                if (response.body().getSuccess().equalsIgnoreCase("true")) {
                    CustomerDetails cutomerDetails = response.body().getResponseData().getCustomerDetails();
                    if ("y".equalsIgnoreCase(cutomerDetails.getProfileComplete())) {
                        //  Log.e("cartSucees__", response.body().getSuccess() + ";;;" + response.body().getMessage());
                        if ("false".equalsIgnoreCase(response.body().getSuccess())) {
                            rvCart.setVisibility(View.GONE);
                            riBottrom.setVisibility(View.GONE);
                            txtEmpty.setVisibility(View.VISIBLE);
                            txtEmpty.setText(response.body().getMessage());
                        } else if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                            // Log.e("cartSucees__", response.body().getSuccess());
                            riBottrom.setVisibility(View.VISIBLE);
                            if (response.body().getResponseData() != null) {
                                adapter.addItems(response.body().getResponseData().getCartItems());
                                rvCart.setAdapter(adapter);
                                rvCart.setVisibility(View.VISIBLE);
                                txtEmpty.setVisibility(View.GONE);
                                Totals totals = response.body().getResponseData().getTotals();
                                txtTotalAmount.setText(getResources().getString(R.string.Rs) + " " + totals.getGrossTotal());
                                txtTotalDiscount.setText(getResources().getString(R.string.Rs) + " " + totals.getTotalDiscount());
                            } else {
                                rvCart.setVisibility(View.GONE);
                                txtEmpty.setVisibility(View.VISIBLE);
                                riBottrom.setVisibility(View.GONE);
                                txtEmpty.setText(response.body().getMessage());
                            }
                        }
                    } else {
                        Intent intent = new Intent(CartActivity.this, MyProfileActivity.class);
                        intent.putExtra("flag", 1);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    txtEmpty.setVisibility(View.VISIBLE);
                    txtEmpty.setText(response.body().getMessage());
                    Toast.makeText(CartActivity.this, response.body().getMessage().toString().trim(), Toast.LENGTH_SHORT);
                }

            }

            @Override
            public void onFailure(Call<UserCartResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    private void deleteCart() {
        showProgress();
        // Log.e("deleteSucees__", customerId);
        Call<DeleteCartResponse> userCartResponseCall = RestApi.get().getRestService().deleteCart(customerId);
        userCartResponseCall.enqueue(new Callback<DeleteCartResponse>() {
            @Override
            public void onResponse(Call<DeleteCartResponse> call, Response<DeleteCartResponse> response) {
                dismissDialog();
                if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                    //    Log.e("deleteSucees__", response.body().getMessage());
                    riBottrom.setVisibility(View.GONE);
                    getCartItems();

                } else {
                    // Log.e("deleteSucees__", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DeleteCartResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    class CartAdapter extends RecyclerView.Adapter<CartHolder> {
        private Context context;
        private List<CartItems> listCartItems = new ArrayList<>();
        int quantity, rate, totalAmount;
        private String cartItemId, productId;

        public CartAdapter(Context context) {
            this.context = context;
        }

        public void addItems(List<CartItems> listCartItems) {
            this.listCartItems = listCartItems;
        }

        @Override
        public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_cart, parent, false);

            return new CartHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final CartHolder holder, final int position) {
            String relatedProducts = new Gson().toJson(listCartItems.get(position).getProductId());
            Log.d("productArray__", relatedProducts);
            holder.getTxtProductTitle().setText(listCartItems.get(position).getName());
            holder.getAmount().setText(listCartItems.get(position).getRate());
            holder.getTxtAmount().setText(getResources().getString(R.string.Rs) + " " + listCartItems.get(position).getTotalAmt());
            holder.getTxtDate().setText(listCartItems.get(position).getCreatedAt());
            holder.getTxtQuantity().setText(listCartItems.get(position).getQty());
            Picasso.with(context).load(listCartItems.get(position).getImages().get(0).getImage())
                    .into(holder.getImProductImz());
            quantity = Integer.parseInt(listCartItems.get(position).getQty());
            rate = (int) Float.parseFloat(listCartItems.get(position).getRate());
            totalAmount = (int) Float.parseFloat(listCartItems.get(position).getTotalAmt());

            holder.getImAddProduct().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("qty", quantity + "rate:::" + rate);
                    quantity = quantity + 1;
                    int qty = Integer.parseInt(listCartItems.get(position).getQty())+1;
                    totalAmount = rate * qty;
                    holder.getTxtQuantity().setText(String.valueOf(qty));
                    holder.getTxtAmount().setText(String.valueOf(totalAmount));
                    productId = listCartItems.get(position).getProductId();
                    Log.e("updateCart__", customerId + ";;;" + productId + ";;;;" + qty + ";;;" + totalAmount);
                    //updateCart(productId,qty);
                    updateCart(productId,1);
                }
            });

            holder.getImDeleteItem().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartItemId = listCartItems.get(position).getId();
                    Log.e("deleteSucees__", cartItemId);
                    deletCartItem();
                }
            });

            holder.getImRemoveProduct().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (quantity == 1) {
                    } else {
                        quantity = quantity - 1;
                        totalAmount = rate * quantity;
                        holder.getTxtQuantity().setText(String.valueOf(quantity));
                        holder.getTxtAmount().setText(String.valueOf(totalAmount));
                        productId = listCartItems.get(position).getProductId();
                        Log.e("updateCart__", customerId + ";;;" + productId + ";;;;" + quantity + ";;;" + totalAmount);
                        //updateCart(productId,quantity);
                        updateCart(productId,-1);
                    }
                }
            });
            holder.getImProductImz().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sp.edit().putString("productId", listCartItems.get(position).getProductId()).commit();
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("flag", 0);
                    startActivity(intent);
                    finish();
                }
            });

            /*segmented2.setTintColor(Color.parseColor("#FFFFFF"), Color.parseColor("#2D6096"));
            rb_daily.setChecked(true);

            segmented2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.rb_daily:
                            loadWinnerList(0);
                            break;
                        case R.id.rb_weekly:
                            loadWinnerList(1);
                            break;
                        case R.id.rb_monthly:
                            loadWinnerList(2);
                            break;

                        default:
                            break;
                    }
                }
            });*/
        }

        private void deletCartItem() {
            showProgress();
            Call<DeleteCartItemResponse> userCartResponseCall = RestApi.get().getRestService().deleteItem(cartItemId);
            userCartResponseCall.enqueue(new Callback<DeleteCartItemResponse>() {
                @Override
                public void onResponse(Call<DeleteCartItemResponse> call, Response<DeleteCartItemResponse> response) {
                    dismissDialog();
                    if (response.isSuccessful()) {
                        if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                            Log.e("deleteSucees__", response.body().getMessage());
                            riBottrom.setVisibility(View.GONE);
                            getCartItems();

                        } else {

                        }
                    }
                }

                @Override
                public void onFailure(Call<DeleteCartItemResponse> call, Throwable t) {
                    dismissDialog();
                }
            });

        }

        private void updateCart(String productId,int quantity) {
            showProgress();
            Call<AddCartResponse> addCartResponseCall = RestApi.get().getRestService().addCart(customerId,
                    productId,
                    String.valueOf(quantity));

            addCartResponseCall.enqueue(new Callback<AddCartResponse>() {
                @Override
                public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                    dismissDialog();
                    if (response.isSuccessful()) {
                        if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                            Log.e("succesResponse", response.body().getMessage());
                            getCartItems();
                        } else {
                            Toast.makeText(CartActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddCartResponse> call, Throwable t) {
                    dismissDialog();
                }
            });
        }

        @Override
        public int getItemCount() {
            return listCartItems.size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent;
                //  Log.e("flag", flag);
                if (flag.equalsIgnoreCase("0")) {
                    homeIntent = new Intent(this, HomeActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                } else {
                    homeIntent = new Intent(this, ProductDetailsActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                }

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent;
        if (flag.equalsIgnoreCase("0")) {
            homeIntent = new Intent(this, HomeActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        } else {
            homeIntent = new Intent(this, ProductDetailsActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        }
    }

}
