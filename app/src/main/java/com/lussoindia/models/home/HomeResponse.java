package com.lussoindia.models.home;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class HomeResponse {
    private List<DisplayItems> displayItems;

    private String success;

    public List<DisplayItems> getDisplayItems ()
    {
        return displayItems;
    }

    public void setDisplayItems (List<DisplayItems> displayItems)
    {
        this.displayItems = displayItems;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [displayItems = "+displayItems+", success = "+success+"]";
    }
}
