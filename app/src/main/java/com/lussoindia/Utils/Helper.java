package com.lussoindia.Utils;


import android.app.Dialog;
import android.content.res.Resources;


public class Helper {

    public final static String DEVICE_TYPE = "Android";
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int GALLERY_PICK_REQUEST_CODE = 200;
    public static final int CAMERA_CAPTURE_CROP_REQUEST_CODE = 300;
    public static final int SELECT_IMAGE_REQUEST_CODE = 400;
    public static final int SELECT_VIDEO_REQUEST_CODE = 500;
    public static final int SELECT_FILE_REQUEST_CODE = 600;
    public static final String MAIN_DIRECTORY_FOLDER = "T360";
    public static final String MAIN_DIRECTORY_NAME = MAIN_DIRECTORY_FOLDER + "/";
    public static final String VIDEO_DIRECTORY_NAME = MAIN_DIRECTORY_NAME + "CAPTURED_VIDEOS";
    public static final String DOWNLOAD_VIDEO_DIRECTORY_NAME = MAIN_DIRECTORY_NAME + "DOWNLOADED_VIDEOS";
    public static final String IMAGE_DIRECTORY_NAME = MAIN_DIRECTORY_NAME + "CAPTURED_IMAGES";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int DEFAULT_BUFFER_SIZE = 1024;
    //
    public static final String CATEGORY_SELECTED = "com.ytrtech.t360.reservation.CATEGORY_SELECTED";
    public static int reqWidth = 320, reqHeight = 240;
    public static int timeOut = 3 * 60 * 1000;
    public static Dialog progress;
    public static int level = -1;
    public static String T360 = "T360";
    public static int FRAGMENT_ADD = 1;
    public static int FRAGMENT_REPLACE = 2;
    public static String BASE_FOLDER =/* "Images";//*/"MobileAPPImagesFolder";
    public static String dashboard_path = BASE_FOLDER + "/Dashboard";
    public static String pack_tour_cat_path = BASE_FOLDER + "/Package-Tour-Category";
    public static String destination_cat_path = BASE_FOLDER + "/Destination-Category";
    public static String destinations_path = BASE_FOLDER + "/Destinations";
    public static String pack_tour_path = BASE_FOLDER + "/Package-Tour";
    public static String pack_schedule_path = BASE_FOLDER + "/Package-Schedule";
    public static String poi_path = BASE_FOLDER + "/POIS";
    public static String events_path = BASE_FOLDER + "/Local-Events";
    public static String products_path = BASE_FOLDER + "/Local-Products";
    public static String cusine_path = BASE_FOLDER + "/Local-Cuisines";
    public static String activities_path = BASE_FOLDER + "/Activities";
    public static String ticket_path = BASE_FOLDER + "/Tickets";
    public static String venues_path = BASE_FOLDER + "/Venues";
    public static String poi_cat_path = BASE_FOLDER + "/POI_Categories";

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public interface OmniKonnectKeys {
        String OMNIKONNECT_GALLERY_IMAGES = "omnikonnect_gallery_images";
        String OMNIKONNECT_HOMESCREEN_SLIDE_IMAGES = "omnikonnect_homescreen_slide_images";
    }


}