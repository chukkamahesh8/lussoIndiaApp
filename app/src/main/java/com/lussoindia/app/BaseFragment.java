package com.lussoindia.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by mahesh on 11/3/2017.
 */

public class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void loadFragment(Activity activity, int fragmentAddOrReplace, Fragment fragmentToLoad, String fragmentTag, Bundle mBundleData, boolean hasBackStack) {

        if (activity instanceof BaseActivity) {
            ((BaseActivity) getActivity()).loadFragment(fragmentAddOrReplace, fragmentToLoad, fragmentTag, mBundleData, hasBackStack);
        } else if (activity instanceof BaseActivity) {
            ((BaseActivity) getActivity()).loadFragment(fragmentAddOrReplace, fragmentToLoad, fragmentTag, mBundleData, hasBackStack);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //setActionBarTitle(this);
    }

    private ProgressDialog progressDialog;

    public void showProgress() {
        showProgress("Please wait..");
    }

    public void showProgress(String title) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
        }
        progressDialog.setMessage(title);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void hideKeyboard() {
        if (getActivity() == null) {
            return;
        }
        InputMethodManager inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = (getActivity()).getCurrentFocus();
        if (v == null) {
            v = new View(getActivity());
            return;
        }

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
