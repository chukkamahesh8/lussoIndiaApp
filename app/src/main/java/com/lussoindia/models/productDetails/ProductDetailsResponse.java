package com.lussoindia.models.productDetails;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class ProductDetailsResponse {
    private String response;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    private String success;

    private ProductDetails productDetails;

    private List<TopRelatedProducts> topRelatedProducts;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public List<TopRelatedProducts> getTopRelatedProducts() {
        return topRelatedProducts;
    }

    public void setTopRelatedProducts(List<TopRelatedProducts> topRelatedProducts) {
        this.topRelatedProducts = topRelatedProducts;
    }

    @Override
    public String toString() {
        return "ClassPojo [response = " + response + ", productDetails = " + productDetails + ", topRelatedProducts = " + topRelatedProducts + "]";
    }
}
