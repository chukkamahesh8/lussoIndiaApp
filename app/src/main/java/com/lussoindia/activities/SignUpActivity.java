package com.lussoindia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.models.getCustomerDetailsByCode.GetCustomerDetailsByCode;
import com.lussoindia.models.register.RegisterResponse;
import com.lussoindia.network.RestApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 5/20/2018.
 */

public class SignUpActivity extends BaseActivity {

    @BindView(R.id.et_UserName)
    EditText etUserName;

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.et_confirm_pwd)
    EditText etConfirmPwd;

    @BindView(R.id.et_Customercode)
    EditText etCustomerCode;

    @BindView(R.id.btn_sign_up)
    Button btnRegister;

    @BindView(R.id.btn_load_c_details)
    ImageView btnCDetails;

    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;
    private String customerId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        toolbar = findViewById(R.id.toolbar);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("Signup");
        ImageView imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);


        btnCDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etUserName.getText().toString().trim())) {
                    etCustomerCode.setError("Please enter Customer Code");
                    etCustomerCode.requestFocus();
                } else {
                    String customerCode = etCustomerCode.getText().toString();
                    customerId = customerCode;
                    Call<GetCustomerDetailsByCode> getCustomerDetailsResponseCall = RestApi.get().getRestService().getCustomerDetailsByCode(customerCode);
                    getCustomerDetailsResponseCall.enqueue(new Callback<GetCustomerDetailsByCode>() {
                        @Override
                        public void onResponse(Call<GetCustomerDetailsByCode> call, Response<GetCustomerDetailsByCode> response) {
                            dismissDialog();
                            if ("true".equalsIgnoreCase(response.body().getsuccess())) {
                                com.lussoindia.models.getCustomerDetailsByCode.CustomerDetails obj = response.body().getCustomerDetails().get(0);
                                if (!TextUtils.isEmpty(obj.getUsername())) {
                                    etUserName.setText(obj.getUsername());
                                }

                                if (!TextUtils.isEmpty(obj.getName())) {
                                    etName.setText(obj.getName());
                                }
                                if (!TextUtils.isEmpty(obj.getPassword())) {
                                    etPassword.setText(obj.getPassword());
                                }
                                if (!TextUtils.isEmpty(obj.getEmail())) {
                                    etEmail.setText(obj.getEmail());
                                }
                                if (!TextUtils.isEmpty(obj.getPhone_no())) {
                                    etPhone.setText(obj.getPhone_no());
                                }
                            } else {
                                Toast.makeText(SignUpActivity.this, response.message().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GetCustomerDetailsByCode> call, Throwable t) {
                            dismissDialog();
                        }
                    });
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etUserName.getText().toString().trim())) {
                    etUserName.setError("Please enter UserName");
                    etUserName.requestFocus();
                } else if (TextUtils.isEmpty(etName.getText().toString().trim())) {
                    etName.setError("Please enter Name");
                    etName.requestFocus();
                } else if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    etEmail.setError("Please enter Email");
                    etEmail.requestFocus();
                } else if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
                    etPhone.setError("Please enter Mobile number");
                    etPhone.requestFocus();
                } else if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                    etPassword.setError("Please enter Password");
                    etPassword.requestFocus();
                } else if (TextUtils.isEmpty(etConfirmPwd.getText().toString().trim())) {
                    etConfirmPwd.setError("Please enter Confirm Password");
                    etConfirmPwd.requestFocus();
                } else if (TextUtils.equals(etPassword.getText().toString().trim(), etConfirmPwd.getText().toString().trim())) {
                    etConfirmPwd.setError("Confirm password should equal to password");
                    etConfirmPwd.requestFocus();
                } else {
                    String username = etUserName.getText().toString();
                    String name = etUserName.getText().toString();
                    String email = etUserName.getText().toString();
                    String phone = etUserName.getText().toString();
                    String password = etUserName.getText().toString();

                    Call<RegisterResponse> registerResponseCall = RestApi.get().getRestService().registration(username,
                            password,
                            name,
                            email,
                            phone, customerId);

                    registerResponseCall.enqueue(
                            new Callback<RegisterResponse>() {
                                @Override
                                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                                    if (response.isSuccessful()) {
                                        if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                                            Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<RegisterResponse> call, Throwable t) {

                                }
                            }
                    );

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:

                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }
}
