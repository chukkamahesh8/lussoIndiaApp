package com.lussoindia.network;

import android.content.Context;
import android.content.SharedPreferences;

import com.lussoindia.Utils.Connections;
import com.lussoindia.models.AddOrderResponse;
import com.lussoindia.models.CustomerOrdersResponse;
import com.lussoindia.models.OrderDetailResponse;
import com.lussoindia.models.SucessReponse;
import com.lussoindia.models.UpdateCustomerProfile;
import com.lussoindia.models.addProductReview.AddProductReviewResponse;
import com.lussoindia.models.cart.DeleteCartItemResponse;
import com.lussoindia.models.cart.DeleteCartResponse;
import com.lussoindia.models.cart.UserCartResponse;
import com.lussoindia.models.cart.addCart.AddCartResponse;
import com.lussoindia.models.categorySearch.CategorySearchResponse;
import com.lussoindia.models.cities.CityResponse;
import com.lussoindia.models.getAddress.GetAddressResponse;
import com.lussoindia.models.getAllCategories.GetAllCategoriesResponse;
import com.lussoindia.models.getCustomerDetails.GetCustomerDetailsResponse;
import com.lussoindia.models.getCustomerDetailsByCode.GetCustomerDetailsByCode;
import com.lussoindia.models.home.HomeResponse;
import com.lussoindia.models.keywordSearch.KeywordSearchResponse;
import com.lussoindia.models.login.LoginResponse;
import com.lussoindia.models.prodcutReview.ProductReview;
import com.lussoindia.models.productDetails.ProductDetailsResponse;
import com.lussoindia.models.register.RegisterResponse;
import com.lussoindia.models.states.StateResponse;
import com.lussoindia.models.updateAddress.UpdateAddress;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * Ref: 1. https://futurestud.io/blog/android-basic-authentication-with-retrofit
 * Created by mahesh on 17/8/16.
 */
public class RestApi {
    public static String BASE_URL = Connections.BASE_URL;
    private static SharedPreferences sp;

    private static RestApi instance = null;


    public static synchronized RestApi get() {
        if (instance == null) {
            instance = new RestApi();
        }
        return instance;
    }

    public static void init(Context context) {
        sp = context.getSharedPreferences("GSS", Context.MODE_PRIVATE);
    }

    public RestService getRestService() {
        if (sp == null) {
            throw new IllegalStateException("init method should be called first.");
        }
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        return buildAdapter(BASE_URL, RestService.class, builder);
    }

    private String getApiToken() {
        //TODO: Need to confirm on Basic or Bearer ?
        final String credentials = "Basic " + sp.getString("apiToken", "");
        //Log.e("apiToken",credentials);
        //final String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        //return base64EncodedCredentials;
        return credentials;
    }

    private <T> T buildAdapter(String baseUrl, Class<T> clazz, OkHttpClient.Builder builder) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //
        OkHttpClient defaultHttpClient = builder
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Content-Type", "application/x-www-form-urlencoded");
                                /*.header("App-Id", "PROSPORT")
                                .header("Client-Type", "APP");*/
                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(defaultHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(clazz);
    }

    private <T> T buildAdapter1(String baseUrl, Class<T> clazz, OkHttpClient.Builder builder) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //
        OkHttpClient defaultHttpClient = builder
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Content-Type", "application/x-www-form-urlencoded");
                                /*.header("App-Id", "PROSPORT")
                                .header("Client-Type", "APP");*/
                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(defaultHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(clazz);
    }

    public interface RestService {
        //Home Serice
        //http://api.lussoindia.com/home
        @FormUrlEncoded
        @POST("home")
        // Call<HomeResponse> getHomeList();
        Call<HomeResponse> getHomeList(@Field("customerId") String customerId);

        @FormUrlEncoded
        @POST("home")
        Call<HomeResponse> getHomeListByUserId(@Field("customerId") String customerId);

        //Login
        //http://api.lussoindia.com/customer/login
        @FormUrlEncoded
        @POST("customer/login")
        Call<LoginResponse> login(@Field("username") String username, @Field("password") String password);

        //Customer Register
        //http://api.lussoindia.com/customer/register
        @FormUrlEncoded
        @POST("customer/register")
        Call<RegisterResponse> registration(@Field("username") String username,
                                            @Field("password") String password,
                                            @Field("name") String name,
                                            @Field("email") String email,
                                            @Field("phone") String phone,
                                            @Field("customerId") String customer_id);

        //GetAddress
        //http://api.lussoindia.com/customer/getadresses
        @FormUrlEncoded
        @POST("customer/getadresses")
        Call<GetAddressResponse> getAddress(@Field("customerId") String customerId);

        //UpdateAddress
        //http://api.lussoindia.com/customer/updateaddress
        @FormUrlEncoded
        @POST("customer/updateaddress")
        Call<UpdateAddress> updateAddress(@Field("customerId") String customerId,
                                          @Field("addressline1") String addressline1,
                                          @Field("addressline2") String addressline2,
                                          @Field("cityId") String cityId,
                                          @Field("taluka") String taluka,
                                          @Field("state") String state,
                                          @Field("zip") String zip
        );

        //Get CustomerDetailsBy Customer code
        //http://api.lussoindia.com/customer/detailsbycode
        @FormUrlEncoded
        @POST("customer/detailsbycode")
        Call<GetCustomerDetailsByCode> getCustomerDetailsByCode(@Field("customerCode") String customerCode);

        //GetCustomerDetails
        //http://api.lussoindia.com/customer/details
        @FormUrlEncoded
        @POST("customer/details")
        Call<GetCustomerDetailsResponse> getCustomerDetails(@Field("customerId") String customerId);

        //updateCustomerProfile
        //http://api.lussoindia.com/customer/addedit
        @FormUrlEncoded
        @POST("customer/addedit")
        Call<UpdateCustomerProfile> updateCustomerDetails(@Field("customerId") String customerId,
                                                          @Field("name") String name,
                                                          @Field("gstin") String gstin,
                                                          @Field("addressline1") String addressline1,
                                                          @Field("addressline2") String addressline2,
                                                          @Field("state") String state,
                                                          @Field("cityId") String cityId,
                                                          @Field("taluka") String taluka,
                                                          @Field("zip") String zip,
                                                          @Field("email") String email,
                                                          @Field("phone") String phone,
                                                          @Field("mobile") String mobile,
                                                          @Field("purchaseManager") String purchaseManager,
                                                          @Field("purchasePhone") String purchasePhone,
                                                          @Field("salesManager") String salesManager,
                                                          @Field("salesPhone") String salesPhone,
                                                          @Field("fnbManager") String fnbManager,
                                                          @Field("fnbPhone") String fnbPhone
        );

        //Add Product Review
        //http://api.lussoindia.com/product/addreview
        @FormUrlEncoded
        @POST("product/addreview")
        Call<AddProductReviewResponse> addProductReviewResponse(@Field("productId") String productId,
                                                                @Field("customerId") String customerId,
                                                                @Field("rating") String rating,
                                                                @Field("review") String review

        );

        //Prodcut Review
        //http://api.lussoindia.com/product/review
        @FormUrlEncoded
        @POST("product/review")
        Call<ProductReview> productReview(@Field("productId") String productId);

        //Product Details
        //http://api.lussoindia.com/product/details
        @FormUrlEncoded
        @POST("product/details")
        Call<ProductDetailsResponse> getProductDetails1(@Field("customerId") String customerId,
                                                        @Field("productId") String productId);

        @FormUrlEncoded
        @POST("product/details")
        Call<ProductDetailsResponse> getProductDetails(@Field("productId") String productId,
                                                       @Field("customerId") String customerId);

        //KeyWordSearch
        //http://api.lussoindia.com/product/search
        @FormUrlEncoded
        @POST("product/search")
        Call<KeywordSearchResponse> keywordSearch(@Field("keyword") String keyword);

        //GetAppCategories
        //http://api.lussoindia.com/categories
        @GET("categories")
        Call<GetAllCategoriesResponse> getAllCategories();

        //CategorySearch
        //http://api.lussoindia.com/product/categorysearch
        @FormUrlEncoded
        @POST("product/categorysearch")
        Call<CategorySearchResponse> categorySearch(@Field("customerId") String customerId,
                                                    @Field("categoryId") String categoryId);


        //UserCart
        //http://api.lussoindia.com/getusercart
        @FormUrlEncoded
        @POST("getusercart")
        Call<UserCartResponse> getUserCart(@Field("userId") String userId);

        //Dlete Cart
        //http://api.lussoindia.com/deletecart
        @FormUrlEncoded
        @POST("deletecart")
        Call<DeleteCartResponse> deleteCart(@Field("customerId") String customerId);

        //UserCart
        //http://api.lussoindia.com/deletecartproduct
        @FormUrlEncoded
        @POST("deletecartproduct")
        Call<DeleteCartItemResponse> deleteItem(@Field("cartItemId") String cartItemId);

        //AddOrder
        //http://api.lussoindia.com/addorder
        @FormUrlEncoded
        @POST("addorder")
        Call<AddOrderResponse> addOrder(@Field("cartItemId") String cartItemId);

        //Add Cart
        //http://api.lussoindia.com/addtocart
        @FormUrlEncoded
        @POST("addtocart")
        Call<AddCartResponse> addCart(@Field("userId") String userId,
                                      @Field("productId") String productId,
                                      @Field("qty") String qty
        );

        //states
        //http://api.lussoindia.com/states
        @POST("states")
        Call<StateResponse> getStates();

        //Cities
        //http://api.lussoindia.com/cities
        @FormUrlEncoded
        @POST("cities")
        Call<CityResponse> getCities(@Field("stateId") String stateId);

        //sendQuote
        //http://api.lussoindia.com/sendquoterequest
        @FormUrlEncoded
        @POST("sendquoterequest")
        Call<SucessReponse> sendQuote(@Field("customerId") String customerId, @Field("productArray") String productArray, @Field("message") String message);

        //CustomerOrders
        //http://api.lussoindia.com/customerorders
        @FormUrlEncoded
        @POST("customerorders")
        Call<List<CustomerOrdersResponse>> getCustomerOrders(@Field("customerId") String customerId);

        //OrderDetails
        //http://api.lussoindia.com/orderdetails
        @FormUrlEncoded
        @POST("orderdetails")
        Call<OrderDetailResponse> orderDetails(@Field("billId") String billId);

    }

}

