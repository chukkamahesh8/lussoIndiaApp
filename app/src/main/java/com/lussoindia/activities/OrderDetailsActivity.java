package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;

import butterknife.BindView;

/**
 * Created by Thriveni on 6/6/2018.
 */

public class OrderDetailsActivity extends BaseActivity {

    String customerId = "";
    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;
    ImageView imMenu;
    SharedPreferences sp = null;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent homeIntent = new Intent(OrderDetailsActivity.this, MyOrdersActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            }
        });

        txtTitle.setText("Order Details");
       /* toolbar = findViewById(R.id.toolbar);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setVisibility(View.GONE);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("Detail Order");
        setSupportActionBar(toolbar);
        ImageView imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        toolbar.getNavigationIcon().setColorFilter(ContextCompat.getColor(OrderDetailsActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
*/
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId =sp.getString("customerId", "");// "7";//sp.getString("customerId", "");

    }

    @Override
    public void onBackPressed() {
        Intent homeIntent;
        homeIntent = new Intent(this, MyOrdersActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent;
                homeIntent = new Intent(this, MyOrdersActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);

        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
