package com.lussoindia.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lussoindia.R;
import com.lussoindia.adapters.*;

import butterknife.BindView;
import ss.com.bannerslider.views.BannerSlider;

/**
 * Created by Thriveni on 6/12/2018.
 */

public class Home1Holder extends com.lussoindia.adapters.AbstractViewHolder {
    public Home1Holder(View view) {
        super(view);
    }

    @BindView(R.id.banners)
    BannerSlider bannerSlider;

    @BindView(R.id.txt_type)
    TextView txtType;

    @BindView(R.id.lv_products)
    ListView lvProducts;

    @BindView(R.id.ll_home_products)
    LinearLayout llHomeProducts;

    @BindView(R.id.ll_see_more)
    LinearLayout llSeeMore;

    @BindView(R.id.im_see_more)
    ImageView imSeeMore;

    public LinearLayout getLlHomeProducts() {
        return llHomeProducts;
    }

    public BannerSlider getBannerSlider() {
        return bannerSlider;
    }

    public TextView getTxtType() {
        return txtType;
    }

    public ListView getLvProducts() {
        return lvProducts;
    }

    public LinearLayout getLlSeeMore() {
        return llSeeMore;
    }

    public ImageView getImSeeMore() {
        return imSeeMore;
    }
}
