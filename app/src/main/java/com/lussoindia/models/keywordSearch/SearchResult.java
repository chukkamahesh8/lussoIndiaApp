package com.lussoindia.models.keywordSearch;

import java.util.List;

/**
 * Created by Thriveni on 5/15/2018.
 */

public class SearchResult {
    private String totalstock;

    private String keywords;

    private String product_id;

    private String mrp;

    private String category_id;

    private String date;

    private String deleted;

    private String minimumOrderQty;

    private String brandId;

    private String product_reorder;

    private String hsncode;

    private String unit;

    private String companyid;

    private String title;

    private String saccode;

    private String ismachine;

    private String tax;

    private String shortDescription;

    private String longDescription;

    private List<Images> images;

    private String rating;

    public String getTotalstock ()
    {
        return totalstock;
    }

    public void setTotalstock (String totalstock)
    {
        this.totalstock = totalstock;
    }

    public String getKeywords ()
    {
        return keywords;
    }

    public void setKeywords (String keywords)
    {
        this.keywords = keywords;
    }

    public String getProduct_id ()
    {
        return product_id;
    }

    public void setProduct_id (String product_id)
    {
        this.product_id = product_id;
    }

    public String getMrp ()
    {
        return mrp;
    }

    public void setMrp (String mrp)
    {
        this.mrp = mrp;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getDeleted ()
    {
        return deleted;
    }

    public void setDeleted (String deleted)
    {
        this.deleted = deleted;
    }

    public String getMinimumOrderQty ()
    {
        return minimumOrderQty;
    }

    public void setMinimumOrderQty (String minimumOrderQty)
    {
        this.minimumOrderQty = minimumOrderQty;
    }

    public String getBrandId ()
    {
        return brandId;
    }

    public void setBrandId (String brandId)
    {
        this.brandId = brandId;
    }

    public String getProduct_reorder ()
    {
        return product_reorder;
    }

    public void setProduct_reorder (String product_reorder)
    {
        this.product_reorder = product_reorder;
    }

    public String getHsncode ()
    {
        return hsncode;
    }

    public void setHsncode (String hsncode)
    {
        this.hsncode = hsncode;
    }

    public String getUnit ()
    {
        return unit;
    }

    public void setUnit (String unit)
    {
        this.unit = unit;
    }

    public String getCompanyid ()
    {
        return companyid;
    }

    public void setCompanyid (String companyid)
    {
        this.companyid = companyid;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getSaccode ()
    {
        return saccode;
    }

    public void setSaccode (String saccode)
    {
        this.saccode = saccode;
    }

    public String getIsmachine ()
    {
        return ismachine;
    }

    public void setIsmachine (String ismachine)
    {
        this.ismachine = ismachine;
    }

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

    public String getShortDescription ()
    {
        return shortDescription;
    }

    public void setShortDescription (String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription ()
    {
        return longDescription;
    }

    public void setLongDescription (String longDescription)
    {
        this.longDescription = longDescription;
    }

    public List<Images> getImages ()
    {
        return images;
    }

    public void setImages (List<Images> images)
    {
        this.images = images;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [totalstock = "+totalstock+", keywords = "+keywords+", product_id = "+product_id+", mrp = "+mrp+", category_id = "+category_id+", date = "+date+", deleted = "+deleted+", minimumOrderQty = "+minimumOrderQty+", brandId = "+brandId+", product_reorder = "+product_reorder+", hsncode = "+hsncode+", unit = "+unit+", companyid = "+companyid+", title = "+title+", saccode = "+saccode+", ismachine = "+ismachine+", tax = "+tax+", shortDescription = "+shortDescription+", longDescription = "+longDescription+", images = "+images+", rating = "+rating+"]";
    }
}
