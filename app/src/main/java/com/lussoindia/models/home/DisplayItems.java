package com.lussoindia.models.home;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class DisplayItems {
    private String type;

    private List<Products> products;

    private List<Banners> banners;

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public List<Products> getProducts ()
    {
        return products;
    }

    public void setProducts (List<Products> products)
    {
        this.products = products;
    }

    public List<Banners> getBanners ()
    {
        return banners;
    }

    public void setBanners (List<Banners> banners)
    {
        this.banners = banners;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [type = "+type+", products = "+products+", banners = "+banners+"]";
    }
}
