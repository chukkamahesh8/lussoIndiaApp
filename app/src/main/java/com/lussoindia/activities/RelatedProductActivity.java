package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.holders.HomeProductsHolder;
import com.lussoindia.holders.RelatedProductsHolder;
import com.lussoindia.models.categorySearch.CategoryProducts;
import com.lussoindia.models.keywordSearch.SearchResult;
import com.lussoindia.models.productDetails.ProductDetailsResponse;
import com.lussoindia.models.productDetails.TopRelatedProducts;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 5/8/2018.
 */

public class RelatedProductActivity extends BaseActivity {
    @BindView(R.id.rv_related_products)
    RecyclerView rvRelatedProducts;

    @BindView(R.id.txt_empty)
    TextView txtEmpty;

    ProductDetailsResponse productDetailsResponse = null;
    RelatedProductsAdapter adapter = null;
    RelatedProductsAdapter1 adapter1 = null;
    RelatedProductsAdapter2 adapter2 = null;
    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;
    int flag = 0;
    ImageView imMenu, imAppLogo;
    private SharedPreferences sp;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_related_products);
        ButterKnife.bind(this);
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);

        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == 0) {
                    Intent homeIntent = new Intent(RelatedProductActivity.this, ProductDetailsActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                } else if (flag == 1) {
                    Intent homeIntent = new Intent(RelatedProductActivity.this, HomeActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                } else if (flag == 2) {
                    Intent homeIntent = new Intent(RelatedProductActivity.this, HomeActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                }
            }
        });
        txtTitle.setText("Related Products");
        /*toolbar = findViewById(R.id.toolbar);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("Related Products");
        ImageView imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.GONE);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setVisibility(View.GONE);
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo);
        imAppLogo.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        toolbar.getNavigationIcon().setColorFilter(ContextCompat.getColor(RelatedProductActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
*/
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvRelatedProducts.setLayoutManager(layoutManager);
        adapter = new RelatedProductsAdapter(this);
        adapter1 = new RelatedProductsAdapter1(this);
        adapter2 = new RelatedProductsAdapter2(this);
        Log.e("onCreate0", "relatedProduct");

        if (getIntent() != null) {
            String relatedProducts = "";
            // if (getIntent().getStringExtra("relatedProducts") != null) {
            //relatedProducts = getIntent().getStringExtra("relatedProducts");
            if (getIntent().getIntExtra("flag", 0) == 0) {

                flag = 0;
                Type type = new TypeToken<List<TopRelatedProducts>>() {
                }.getType();
                if (!TextUtils.isEmpty(sp.getString("relatedProducts", ""))) {
                   // Log.e("RelatedProduct", "From Product details");
                    List<TopRelatedProducts> topRelatedProductsList = new Gson().fromJson(sp.getString("relatedProducts", ""), type);
                    // List<TopRelatedProducts> topRelatedProductsList = sp.getString("relatedProducts",""));//new Gson().fromJson(getIntent().getStringExtra("relatedProducts"), type);
                    txtEmpty.setVisibility(View.GONE);
                    rvRelatedProducts.setVisibility(View.VISIBLE);
                    if (topRelatedProductsList != null && topRelatedProductsList.size() > 0) {
                        adapter.addItems(topRelatedProductsList);
                        rvRelatedProducts.setAdapter(adapter);
                    }
                }

            } else if (getIntent().getIntExtra("flag", 0) == 1) {
                flag = 1;
                Type type = new TypeToken<List<SearchResult>>() {
                }.getType();
                List<SearchResult> topRelatedProductsList = new Gson().fromJson(sp.getString("relatedProducts", ""), type);

                // List<SearchResult> topRelatedProductsList = new Gson().fromJson(getIntent().getStringExtra("relatedProducts"), type);
                txtEmpty.setVisibility(View.GONE);
                rvRelatedProducts.setVisibility(View.VISIBLE);
                if (topRelatedProductsList != null && topRelatedProductsList.size() > 0) {
                    adapter1.addItems(topRelatedProductsList);
                    rvRelatedProducts.setAdapter(adapter1);
                }
            } else if (getIntent().getIntExtra("flag", 0) == 2) {
                flag = 2;
               // Log.e("From Cart Category", "Cart Category");
                Type type = new TypeToken<List<CategoryProducts>>() {
                }.getType();
                List<CategoryProducts> topRelatedProductsList = new Gson().fromJson(sp.getString("relatedProducts", ""), type);

                //List<CategoryProducts> topRelatedProductsList = new Gson().fromJson(getIntent().getStringExtra("relatedProducts"), type);
                txtEmpty.setVisibility(View.GONE);
                rvRelatedProducts.setVisibility(View.VISIBLE);
                if (topRelatedProductsList != null && topRelatedProductsList.size() > 0) {
                    //Log.e("From Cart Category", "Cart Category");
                    adapter2.addItems(topRelatedProductsList);
                    rvRelatedProducts.setAdapter(adapter2);
                }
            }


            /*} else {
                txtEmpty.setVisibility(View.VISIBLE);
                rvRelatedProducts.setVisibility(View.GONE);
            }*/
        }
    }

    class RelatedProductsAdapter extends RecyclerView.Adapter<HomeProductsHolder> {
        public Context context;
        public List<TopRelatedProducts> topRelatedProducts = new ArrayList<>();

        public RelatedProductsAdapter(Context context) {
            this.context = context;
        }

        public void addItems(List<TopRelatedProducts> topRelatedProducts) {
            this.topRelatedProducts = topRelatedProducts;
        }

        @Override
        public HomeProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_products, parent, false);

            return new HomeProductsHolder(itemView);
        }

        @Override
        public void onBindViewHolder(HomeProductsHolder holder, final int position) {
            holder.getTxtProductTitle().setText(topRelatedProducts.get(position).getTitle());
            holder.getTxtProductDesc().setText(topRelatedProducts.get(position).getShortDescription());
            holder.getGetTxtProductMrp().setText(topRelatedProducts.get(position).getMrp());
            Picasso.with(context).load(topRelatedProducts.get(position).getImages().get(0).getImage())
                    .into(holder.getImProduct());
            holder.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    sp.edit().putString("productId", topRelatedProducts.get(position).getProduct_id()).commit();
                    // intent.putExtra("productId", topRelatedProducts.get(position).getProduct_id());
                    intent.putExtra("flag", 1);
                    startActivity(intent);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return topRelatedProducts.size();
        }
    }

    class RelatedProductsAdapter1 extends RecyclerView.Adapter<HomeProductsHolder> {
        public Context context;
        public List<SearchResult> searchResultsList = new ArrayList<>();

        public RelatedProductsAdapter1(Context context) {
            this.context = context;
        }

        public void addItems(List<SearchResult> searchResultsList) {
            this.searchResultsList = searchResultsList;
        }

        @Override
        public HomeProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_products, parent, false);

            return new HomeProductsHolder(itemView);
        }

        @Override
        public void onBindViewHolder(HomeProductsHolder holder, final int position) {
            holder.getTxtProductTitle().setText(searchResultsList.get(position).getTitle());
            holder.getTxtProductDesc().setText(searchResultsList.get(position).getShortDescription());
            holder.getGetTxtProductMrp().setText(searchResultsList.get(position).getMrp());
            Picasso.with(context).load(searchResultsList.get(position).getImages().get(0).getImage())
                    .into(holder.getImProduct());
            holder.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    sp.edit().putString("productId", searchResultsList.get(position).getProduct_id()).commit();
                    intent.putExtra("productId", searchResultsList.get(position).getProduct_id());
                    startActivity(intent);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return searchResultsList.size();
        }
    }

    class RelatedProductsAdapter2 extends RecyclerView.Adapter<HomeProductsHolder> {
        public Context context;
        public List<CategoryProducts> searchResultsList = new ArrayList<>();

        public RelatedProductsAdapter2(Context context) {
            this.context = context;
        }

        public void addItems(List<CategoryProducts> searchResultsList) {
            this.searchResultsList = searchResultsList;

        }

        @Override
        public HomeProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_products, parent, false);

            return new HomeProductsHolder(itemView);
        }

        @Override
        public void onBindViewHolder(HomeProductsHolder holder, final int position) {
           // Log.e("categoryList", "catList" + searchResultsList.size() + searchResultsList.get(position).getTitle());
            holder.getTxtProductTitle().setText(searchResultsList.get(position).getTitle());
            holder.getTxtProductDesc().setText(searchResultsList.get(position).getShortDescription());
            holder.getGetTxtProductMrp().setText(searchResultsList.get(position).getMrp());
            Picasso.with(context).load(searchResultsList.get(position).getImages().get(0).getImage())
                    .into(holder.getImProduct());
            holder.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  Log.e("productId", "productId is::::" + searchResultsList.get(position).getProduct_id());
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    sp.edit().putString("productId", searchResultsList.get(position).getProduct_id()).commit();
                    intent.putExtra("productId", searchResultsList.get(position).getProduct_id());
                    startActivity(intent);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return searchResultsList.size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                if (flag == 0) {
                    Intent homeIntent = new Intent(this, ProductDetailsActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                } else if (flag == 1) {
                    Intent homeIntent = new Intent(this, HomeActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                } else if (flag == 2) {
                    Intent homeIntent = new Intent(this, HomeActivity.class);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        if (flag == 0) {
            Intent homeIntent = new Intent(this, ProductDetailsActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        } else if (flag == 1) {
            Intent homeIntent = new Intent(this, HomeActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        } else if (flag == 2) {
            Intent homeIntent = new Intent(this, HomeActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        }
    }
}
