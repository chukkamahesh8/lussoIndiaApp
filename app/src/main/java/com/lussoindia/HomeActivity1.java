package com.lussoindia;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.lussoindia.adapters.AbstractBaseAdapter;
import com.lussoindia.holders.Home1Holder;
import com.lussoindia.models.home.DisplayItems;
import com.lussoindia.models.home.HomeResponse;
import com.lussoindia.network.RestApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 6/12/2018.
 */

public class HomeActivity1 extends AppCompatActivity {
    @BindView(R.id.lv_home)
    ListView lvHome;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        ButterKnife.bind(this);

        getHome();
    }

    private void getHome() {
        //Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeList();
        Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeList("");
        getHomeList.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {

                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
            }
        });
    }

    /*class HomeAdapter extends AbstractBaseAdapter<DisplayItems, Home1Holder> {

        public HomeAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return 0;
        }

        @Override
        public Home1Holder getViewHolder(View convertView) {
            return null;
        }

        @Override
        public void bindView(int position, Home1Holder holder, DisplayItems item) {

        }
    }*/
}
