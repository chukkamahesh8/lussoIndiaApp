package com.lussoindia.models.cart;

public class DeleteCartItemResponse {

    private String message;

    private String responseData;

    private String success;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getResponseData ()
    {
        return responseData;
    }

    public void setResponseData (String responseData)
    {
        this.responseData = responseData;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", responseData = "+responseData+", success = "+success+"]";
    }

}
