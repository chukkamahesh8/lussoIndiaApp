package com.lussoindia.models.addProductReview;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class AddProductReviewResponse {
    private String message;

    private String response;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    private String success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", response = " + response + "]";
    }
}
