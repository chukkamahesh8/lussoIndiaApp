package com.lussoindia.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lussoindia.R;

import butterknife.BindView;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class HomeProductsHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.im_product)
    ImageView imProduct;

    @BindView(R.id.txt_product_title)
    TextView txtProductTitle;

    @BindView(R.id.txt_product_desc)
    TextView txtProductDesc;

    @BindView(R.id.txt_product_mrp)
    TextView getTxtProductMrp;

    public ImageView getImProduct() {
        return imProduct;
    }

    public TextView getTxtProductTitle() {
        return txtProductTitle;
    }

    public TextView getTxtProductDesc() {
        return txtProductDesc;
    }

    public TextView getGetTxtProductMrp() {
        return getTxtProductMrp;
    }


    public HomeProductsHolder(View itemView) {
        super(itemView);
    }
}
