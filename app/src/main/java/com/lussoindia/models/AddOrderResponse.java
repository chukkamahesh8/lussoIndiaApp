package com.lussoindia.models;

public class AddOrderResponse {

    private String message;

    private String success;

    private String orderId;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    public String getOrderId ()
    {
        return orderId;
    }

    public void setOrderId (String orderId)
    {
        this.orderId = orderId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", success = "+success+", orderId = "+orderId+"]";
    }

}
