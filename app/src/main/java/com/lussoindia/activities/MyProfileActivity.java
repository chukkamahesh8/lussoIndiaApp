package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.models.UpdateCustomerProfile;
import com.lussoindia.models.cities.Cities;
import com.lussoindia.models.cities.CityResponse;
import com.lussoindia.models.getCustomerDetails.CustomerDetails;
import com.lussoindia.models.getCustomerDetails.GetCustomerDetailsResponse;
import com.lussoindia.models.states.StateResponse;
import com.lussoindia.models.states.States;
import com.lussoindia.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 5/23/2018.
 */

public class MyProfileActivity extends BaseActivity {
    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;

    @BindView(R.id.et_p_name)
    EditText etName;

    @BindView(R.id.et_pwd)
    EditText etPassword;

    @BindView(R.id.et_gstin)
    EditText etGstin;

    @BindView(R.id.et_addressline1)
    EditText etAddressLine1;

    @BindView(R.id.et_addressline2)
    EditText etAddressline2;

    @BindView(R.id.sp_states)
    Spinner spState;

    @BindView(R.id.sp_cities)
    Spinner spCities;

    @BindView(R.id.et_taluka)
    EditText etTaluka;

    @BindView(R.id.et_zipcode)
    EditText etZipCode;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_mobile)
    EditText etMobile;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_purchange_manager)
    EditText etPurchageManager;

    @BindView(R.id.et_purchange_manager_mobile)
    EditText etPurchaseManagerMobile;

    @BindView(R.id.et_sales_manager)
    EditText etSalesManager;

    @BindView(R.id.et_sales_manager_mobile)
    EditText etSalesManagerMobile;

    @BindView(R.id.et_fnb_manager)
    EditText etFnbManager;

    @BindView(R.id.et_fnb_manager_mobile)
    EditText etFnbManagerMobile;

    private String customerId = null, cityID = "", cityName, stateName;
    private SharedPreferences sp = null;

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    int flag = 0;
    private String stateId;
    List<States> statesArray = new ArrayList<>();
    List<Cities> citiesArray = new ArrayList<>();
    ImageView imMenu, imAppLogo;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);
        ButterKnife.bind(this);

        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(MyProfileActivity.this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            }
        });

        txtTitle.setText("My Profile");
        /*toolbar = findViewById(R.id.toolbar);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo);
        imMenu.setVisibility(View.GONE);
        imAppLogo.setVisibility(View.GONE);
        ImageView imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.GONE);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("My Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        toolbar.getNavigationIcon().setColorFilter(ContextCompat.getColor(MyProfileActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
*/
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId = sp.getString("customerId", "");
        getStates();
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statesArray.size() > 0) {
                    stateId = statesArray.get(position).getId();
                    stateName = statesArray.get(position).getId();
                    getCities(statesArray.get(position).getId());
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // getCities(spState.getItemAtPosition(position).toString());
                if (citiesArray.size() > 0) {
                    cityID = citiesArray.get(position).getCity_id();
                    cityName = citiesArray.get(position).getCity_name();
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (getIntent() != null) {
            flag = getIntent().getIntExtra("flag", 0);
          //  Log.d("flag", String.valueOf(flag));
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateCustomerProfile();
            }
        });


    }

    private void updateCustomerProfile() {
        if (etName.getText().toString().isEmpty()) {
            etName.setError("enter a valid Name");
            requestFocus(etName);
        } /* else if (etGstin.getText().toString().isEmpty()) {
            etGstin.setError("enter GST No");
            requestFocus(etGstin);
        } */ else if (etAddressLine1.getText().toString().isEmpty()) {
            etAddressLine1.setError("enter a valid AddressLine1");
            requestFocus(etAddressLine1);
        }/* else if (etAddressline2.getText().toString().isEmpty()) {
            etAddressline2.setError("enter a valid AddressLine2");
            requestFocus(etAddressline2);
        }*/ else if (etTaluka.getText().toString().isEmpty()) {
            etTaluka.setError("enter a valid Taluka");
            requestFocus(etTaluka);
        } else if (etZipCode.getText().toString().isEmpty() || etZipCode.getText().toString().length() < 5 || etZipCode.getText().toString().length() > 7) {
            etZipCode.setError("enter valid postal code");
            requestFocus(etZipCode);
        } else if (etEmail.getText().toString().isEmpty()) {
            etEmail.setError("enter a valid Email");
            requestFocus(etEmail);
        } else if (etPhone.getText().toString().isEmpty() || etPhone.getText().toString().length() < 9 || etPhone.getText().toString().length() > 12) {
            etPhone.setError("enter valid phone no");
            requestFocus(etPhone);
        } else if ("Select State".equalsIgnoreCase(stateName)) {
            Toast.makeText(MyProfileActivity.this, "select State", Toast.LENGTH_SHORT).show();
            requestFocus(etPhone);
        } else if ("Select City".equalsIgnoreCase(cityName)) {
            Toast.makeText(MyProfileActivity.this, "select City", Toast.LENGTH_SHORT).show();
            requestFocus(etPhone);
        } /*else if (etMobile.getText().toString().isEmpty() || etMobile.getText().toString().length() < 9 || etMobile.getText().toString().length() > 11) {
            etMobile.setError("enter valid phone no");
            requestFocus(etMobile);
        } else if (etPurchageManager.getText().toString().isEmpty()) {
            etPurchageManager.setError("enter purchage manager name");
            requestFocus(etPurchageManager);
        } else if (etPurchaseManagerMobile.getText().toString().isEmpty() || etPurchaseManagerMobile.getText().toString().length() < 9 || etPurchaseManagerMobile.getText().toString().length() > 11) {
            etPurchaseManagerMobile.setError("enter valid purchage manager mobile no");
            requestFocus(etPurchaseManagerMobile);
        } else if (etSalesManager.getText().toString().isEmpty()) {
            etSalesManager.setError("enter sales manager name");
            requestFocus(etSalesManager);
        } else if (etSalesManagerMobile.getText().toString().isEmpty()) {
            etSalesManagerMobile.setError("enter a valid sales manager mobile");
            requestFocus(etSalesManagerMobile);
        } else if (etFnbManager.getText().toString().isEmpty()) {
            etFnbManager.setError("enter Fnb Manager");
            requestFocus(etFnbManager);
        }  else if (etFnbManagerMobile.getText().toString().isEmpty() || etFnbManagerMobile.getText().toString().length() < 9 || etFnbManagerMobile.getText().toString().length() > 11) {
            etFnbManagerMobile.setError("enter valid fnb manager mobile no");
            requestFocus(etFnbManagerMobile);
        }*/ else {
            showProgress();
            etEmail.setError(null);
            showProgress();
            Call<UpdateCustomerProfile> updateCustomerProfileCall = RestApi.get().getRestService().updateCustomerDetails(customerId,
                    etName.getText().toString().trim(),
                    etGstin.getText().toString().trim(),
                    etAddressLine1.getText().toString().trim(),
                    etAddressline2.getText().toString().trim(),
                    stateId,
                    cityID,
                    etTaluka.getText().toString().trim(),
                    etZipCode.getText().toString().trim(),
                    etEmail.getText().toString().trim(),
                    etPhone.getText().toString().trim(),
                    etMobile.getText().toString().trim(),
                    etPurchageManager.getText().toString().trim(),
                    etPurchaseManagerMobile.getText().toString().trim(),
                    etSalesManager.getText().toString().trim(),
                    etSalesManagerMobile.getText().toString().trim(),
                    etFnbManager.getText().toString().trim(),
                    etFnbManagerMobile.getText().toString().trim()
            );

            updateCustomerProfileCall.enqueue(new Callback<UpdateCustomerProfile>() {
                @Override
                public void onResponse(Call<UpdateCustomerProfile> call, Response<UpdateCustomerProfile> response) {
                    dismissDialog();
                    if (response.isSuccessful()) {
                        if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                            Toast.makeText(MyProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                          //  Log.d("flag__", String.valueOf(flag));
                            if (flag == 1) {
                                Intent intent = new Intent(MyProfileActivity.this, CartActivity.class);
                                intent.putExtra("flag", 0);
                                startActivity(intent);
                                finish();
                            } else {
                                getCustomerDetails(customerId, statesArray);
                            }
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<UpdateCustomerProfile> call, Throwable t) {
                    dismissDialog();
                }
            });
        }
    }

    private void getCustomerDetails(String customerId, final List<States> states) {
        Call<GetCustomerDetailsResponse> getCustomerDetailsResponseCall = RestApi.get().getRestService().getCustomerDetails(customerId);
        getCustomerDetailsResponseCall.enqueue(new Callback<GetCustomerDetailsResponse>() {
            @Override
            public void onResponse(Call<GetCustomerDetailsResponse> call, Response<GetCustomerDetailsResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        dismissDialog();
                        CustomerDetails obj = response.body().getCustomerDetails().get(0);

                        if (!TextUtils.isEmpty(obj.getName())) {
                            etName.setText(obj.getCustomerName());
                        }
                        if (!TextUtils.isEmpty(obj.getPassword())) {
                            etPassword.setText(obj.getPassword());
                        }

                        if (!TextUtils.isEmpty(obj.getGstin())) {
                            etGstin.setText(obj.getGstin());
                        }

                        if (!TextUtils.isEmpty(obj.getAddr_line1())) {
                            etAddressLine1.setText(obj.getAddr_line1());
                        }
                        if (!TextUtils.isEmpty(obj.getAddr_line2())) {
                            etAddressline2.setText(obj.getAddr_line2());
                        }
                        if (!TextUtils.isEmpty(obj.getTaluka())) {
                            etTaluka.setText(obj.getTaluka());
                        }
                        if (!TextUtils.isEmpty(obj.getZip())) {
                            etZipCode.setText(obj.getZip());
                        }
                        if (!TextUtils.isEmpty(obj.getEmail())) {
                            etEmail.setText(obj.getEmail());
                        }
                        if (!TextUtils.isEmpty(obj.getPhone_no())) {
                            etPhone.setText(obj.getPhone_no());
                        }
                        if (!TextUtils.isEmpty(obj.getMobile_no())) {
                            etMobile.setText(obj.getMobile_no());
                        }
                        if (!TextUtils.isEmpty(obj.getPurchase_manager())) {
                            etPurchageManager.setText(obj.getPurchase_manager());
                        }
                        if (!TextUtils.isEmpty(obj.getPurchase_phone())) {
                            etPurchaseManagerMobile.setText(obj.getPurchase_phone());
                        }
                        if (!TextUtils.isEmpty(obj.getSales_manager())) {
                            etSalesManager.setText(obj.getSales_manager());
                        }
                        if (!TextUtils.isEmpty(obj.getSales_phone())) {
                            etSalesManagerMobile.setText(obj.getSales_phone());
                        }
                        if (!TextUtils.isEmpty(obj.getFnb_manager())) {
                            etFnbManager.setText(obj.getFnb_manager());
                        }
                        if (!TextUtils.isEmpty(obj.getFnb_phone())) {
                            etFnbManagerMobile.setText(obj.getFnb_phone());
                        }

                        int customerCityPos = 0;
                        if (!TextUtils.isEmpty(obj.getStateId())) {
                            if (states.size() > 0) {
                                for (int i = 0; i < states.size(); i++) {
                                    if (obj.getStateId().equalsIgnoreCase(states.get(i).getId())) {
                                        customerCityPos = i;

                                    }
                                }
                                stateId = statesArray.get(customerCityPos).getId();
                                spState.setSelection(customerCityPos);
                            }
                        }

//                        Log.e("cityId", obj.getCity_id());
                        if (!TextUtils.isEmpty(obj.getCity_id())) {
                            cityID = obj.getCity_id();
                        }
                        if (!TextUtils.isEmpty(obj.getStateId())) {
                            getCities(obj.getStateId());
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<GetCustomerDetailsResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    public void getStates() {
        showProgress();
        Call<StateResponse> getResponse = RestApi.get().getRestService().getStates();
        getResponse.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                // dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        if (response.body().getStates().size() > 0) {
                            States res = new States();
                            res.setName("Select State");
                            response.body().getStates().add(0, res);
                            statesArray = response.body().getStates();
                            StateSpinnerAdapter adapter = new StateSpinnerAdapter(MyProfileActivity.this,
                                    R.layout.item_spinner_text, response.body().getStates());
                            spState.setAdapter(adapter);

                            getCustomerDetails(customerId, statesArray);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    public void getCities(String stateId) {
       // Log.e("cityId in getCiti()", cityID);
        showProgress();
        Call<CityResponse> getResponse = RestApi.get().getRestService().getCities(stateId);
        getResponse.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        //  Log.e("state response", "response");
                        int customerCityPos = 0;
                        if (response.body().getCities().size() > 0) {
                            Cities res = new Cities();
                            res.setCity_name("Select City");
                            response.body().getCities().add(0, res);
                            citiesArray = response.body().getCities();
                            CitySpinnerAdapter adapter = new CitySpinnerAdapter(MyProfileActivity.this,
                                    R.layout.item_spinner_text, citiesArray);
                            spCities.setAdapter(adapter);
                            if (!TextUtils.isEmpty(cityID)) {
                                if (citiesArray.size() > 0) {
                                    for (int i = 0; i < citiesArray.size(); i++) {
                                        if (cityID.equalsIgnoreCase(citiesArray.get(i).getCity_id())) {
                                            spCities.setSelection(i);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        CitySpinnerAdapter adapter = new CitySpinnerAdapter(MyProfileActivity.this,
                                R.layout.item_spinner_text, citiesArray);
                        adapter.notifyDataSetChanged();
                        spCities.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    public class StateSpinnerAdapter extends ArrayAdapter<States> {
        private LayoutInflater mInflater;
        private Context mContext;
        private List<States> objects = new ArrayList<>();
        private int mResource;

        public StateSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<States> objects) {
            super(context, resource, objects);
            mContext = context;
            mInflater = LayoutInflater.from(context);
            mResource = resource;
            this.objects = objects;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView,
                                    @NonNull ViewGroup parent) {
            return createItemView(position, convertView, parent);
        }

        @Override
        public @NonNull
        View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return createItemView(position, convertView, parent);
        }

        private View createItemView(int position, View convertView, ViewGroup parent) {
            final View view = mInflater.inflate(mResource, parent, false);

            TextView offTypeTv = (TextView) view.findViewById(R.id.txt_spinner_text);

            States stateData = objects.get(position);

            offTypeTv.setText(stateData.getName());

            return view;
        }
    }

    public class CitySpinnerAdapter extends ArrayAdapter<Cities> {
        private LayoutInflater mInflater;
        private Context mContext;
        private List<Cities> objects = new ArrayList<>();
        private int mResource;

        public CitySpinnerAdapter(@NonNull Context context, int resource, @NonNull List<Cities> objects) {
            super(context, resource, objects);
            mContext = context;
            mInflater = LayoutInflater.from(context);
            mResource = resource;
            this.objects = objects;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView,
                                    @NonNull ViewGroup parent) {
            return createItemView(position, convertView, parent);
        }

        @Override
        public @NonNull
        View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return createItemView(position, convertView, parent);
        }

        private View createItemView(int position, View convertView, ViewGroup parent) {
            final View view = mInflater.inflate(mResource, parent, false);

            TextView offTypeTv = (TextView) view.findViewById(R.id.txt_spinner_text);

            Cities cityData = objects.get(position);

            offTypeTv.setText(cityData.getCity_name());

            return view;
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
/*

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:

                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);

        }
        return (super.onOptionsItemSelected(menuItem));
    }
*/

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }
}
