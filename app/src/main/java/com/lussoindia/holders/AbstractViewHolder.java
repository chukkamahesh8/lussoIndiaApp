package com.lussoindia.holders;

import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by Ramesh on 10/02/16.
 */
public abstract class AbstractViewHolder {

    private final View view;

    public AbstractViewHolder(View view) {
        this.view = view;
        ButterKnife.bind(this, view);
    }

    public View getRootView() {
        return view;
    }
}
