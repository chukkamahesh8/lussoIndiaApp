package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.holders.Myordersholder;
import com.lussoindia.models.CustomerOrdersResponse;
import com.lussoindia.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 6/6/2018.
 */

public class MyOrdersActivity extends BaseActivity {
    @BindView(R.id.rv_myorders)
    RecyclerView rvMyOrders;

    @BindView(R.id.txt_empty)
    TextView txtEmpty;

    String customerId = "";
    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;
    ImageView imMenu,imAppLogo;
    SharedPreferences sp = null;
    MyodersAdapter adapter = null;
    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorders);
        ButterKnife.bind(this);

        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(MyOrdersActivity.this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            }
        });

        txtTitle.setText("Orders");
       /* toolbar = findViewById(R.id.toolbar);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setVisibility(View.GONE);
        ImageView imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.GONE);
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo);
        imAppLogo.setVisibility(View.GONE);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("Orders");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        toolbar.getNavigationIcon().setColorFilter(ContextCompat.getColor(MyOrdersActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
*/
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId =sp.getString("customerId", "");//

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvMyOrders.setLayoutManager(layoutManager);
        adapter = new MyodersAdapter(MyOrdersActivity.this);
        getOrders(customerId);


    }

    private void getOrders(String customerId) {
        showProgress();
        Call<List<CustomerOrdersResponse>> customerOrdersResponseCall = RestApi.get().getRestService().getCustomerOrders(customerId);
        customerOrdersResponseCall.enqueue(new Callback<List<CustomerOrdersResponse>>() {
            @Override
            public void onResponse(Call<List<CustomerOrdersResponse>> call, Response<List<CustomerOrdersResponse>> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        txtEmpty.setVisibility(View.GONE);
                        rvMyOrders.setVisibility(View.VISIBLE);
                        adapter.addItems(response.body());
                        rvMyOrders.setAdapter(adapter);

                    } else {
                        txtEmpty.setVisibility(View.VISIBLE);
                        rvMyOrders.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onFailure(Call<List<CustomerOrdersResponse>> call, Throwable t) {
dismissDialog();
            }
        });
    }

    class MyodersAdapter extends RecyclerView.Adapter<Myordersholder> {
        Context context;
        List<CustomerOrdersResponse> myOrdersList = new ArrayList<>();

        public MyodersAdapter(Context context) {
            this.context = context;
        }

        public void addItems(List<CustomerOrdersResponse> myOrdersList) {
            this.myOrdersList = myOrdersList;
        }

        @Override
        public Myordersholder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_myorder, parent, false);

            return new Myordersholder(itemView);
        }

        @Override
        public void onBindViewHolder(Myordersholder holder, final int position) {
            holder.getTxtBillNo().setText(myOrdersList.get(position).getBillnumber());
            holder.getTxtTxnNo().setText(myOrdersList.get(position).getTaxtype());
            holder.getTxtBillDate().setText(myOrdersList.get(position).getBilldate());
            holder.getTxtBillTotal().setText(myOrdersList.get(position).getBilltotal());
            holder.getTxtTaxTotal().setText(myOrdersList.get(position).getTaxtotal());
            holder.getTxtCustomer().setText(myOrdersList.get(position).getCustomerName());
            holder.getTxtGstNo().setText(myOrdersList.get(position).getCustgstno());
            holder.getImOrderDetails().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Intent intent = new Intent(MyOrdersActivity.this, OrderDetailsActivity.class);
                    intent.putExtra("orderid", myOrdersList.get(position).getId());
                    context.startActivity(intent);
                    finish();*/
                }
            });
        }

        @Override
        public int getItemCount() {
            return myOrdersList.size();
        }
    }


    @Override
    public void onBackPressed() {
        Intent homeIntent;
        homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent;
                homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);

        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
