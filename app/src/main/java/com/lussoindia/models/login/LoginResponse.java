package com.lussoindia.models.login;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class LoginResponse {
    private String message;

    private String customerId;

    private String success;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCustomerId ()
    {
        return customerId;
    }

    public void setCustomerId (String customerId)
    {
        this.customerId = customerId;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", customerId = "+customerId+", success = "+success+"]";
    }
}
