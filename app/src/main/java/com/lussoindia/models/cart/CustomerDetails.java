package com.lussoindia.models.cart;

public class CustomerDetails {

    private String customerName;

    private String customerGstNo;

    private String customerAddress;

    private String profileComplete;

    private String customerPhone;

    public String getCustomerName ()
    {
        return customerName;
    }

    public void setCustomerName (String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerGstNo ()
    {
        return customerGstNo;
    }

    public void setCustomerGstNo (String customerGstNo)
    {
        this.customerGstNo = customerGstNo;
    }

    public String getCustomerAddress ()
    {
        return customerAddress;
    }

    public void setCustomerAddress (String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getProfileComplete ()
    {
        return profileComplete;
    }

    public void setProfileComplete (String profileComplete)
    {
        this.profileComplete = profileComplete;
    }

    public String getCustomerPhone ()
    {
        return customerPhone;
    }

    public void setCustomerPhone (String customerPhone)
    {
        this.customerPhone = customerPhone;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [customerName = "+customerName+", customerGstNo = "+customerGstNo+", customerAddress = "+customerAddress+", profileComplete = "+profileComplete+", customerPhone = "+customerPhone+"]";
    }

}
