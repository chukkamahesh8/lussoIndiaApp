package com.lussoindia.models.prodcutReview;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class Reviews {
    private String customerName;

    private String customerState;

    private String customerCity;

    private String rating;

    private String review;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerState() {
        return customerState;
    }

    public void setCustomerState(String customerState) {
        this.customerState = customerState;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    @Override
    public String toString() {
        return "ClassPojo [customerName = " + customerName + ", customerState = " + customerState + ", customerCity = " + customerCity + ", rating = " + rating + ", review = " + review + "]";
    }
}
