package com.lussoindia.models.getAddress;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class GetAddressResponse {
    private String response;

    private List<Addresses> addresses;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "ClassPojo [response = " + response + ", addresses = " + addresses + "]";
    }
}
