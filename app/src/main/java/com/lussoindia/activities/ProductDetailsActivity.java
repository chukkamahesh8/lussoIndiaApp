package com.lussoindia.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.models.SucessReponse;
import com.lussoindia.models.cart.addCart.AddCartResponse;
import com.lussoindia.models.productDetails.Images;
import com.lussoindia.models.productDetails.ProductDetails;
import com.lussoindia.models.productDetails.ProductDetailsResponse;
import com.lussoindia.network.RestApi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

/**
 * Created by Thriveni on 5/3/2018.
 */

public class ProductDetailsActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.product_images_banner)
    BannerSlider productImagesBanner;
    //ImageView productImagesBanner;

    @BindView(R.id.txt_product_title)
    TextView txtProductTitle;

    @BindView(R.id.txt_product_desc)
    TextView txtProductDesc;

    @BindView(R.id.txt_product_price)
    TextView txtProdcutPrice;

    @BindView(R.id.txt_quantity)
    TextView txtQuantity;

    @BindView(R.id.im_add_product)
    ImageView imAddProduct;

    @BindView(R.id.im_remove_product)
    ImageView imRemoveProduct;

    @BindView(R.id.txt_empty)
    TextView txtEmpty;

    @BindView(R.id.ll_product_details)
    LinearLayout llProductDetails;

    @BindView(R.id.ll_about_product)
    LinearLayout llAboutProduct;

    @BindView(R.id.im_about_product)
    ImageView imAboutProduct;

    @BindView(R.id.ll_customer_reviews)
    LinearLayout llCustomerReviews;

    @BindView(R.id.im_customer_reviews)
    ImageView imCustomerReviews;

    @BindView(R.id.ll_related_products)
    LinearLayout llRelatedProducts;

    @BindView(R.id.im_related_products)
    ImageView imRelatedProducts;

    @BindView(R.id.txt_product_mrp)
    TextView txtProductMrp;

    @BindView(R.id.txt_product_saveed_price)
    TextView txt_product_saveed_price;

    @BindView(R.id.txt_delivery_charge_price)
    TextView txt_delivery_charge_price;

    @BindView(R.id.txt_min_order)
    TextView txt_min_order;

    private int totalQuantity = 1, orderQtyLimit = 10;

    private SharedPreferences sp = null;
    private String customerId = "";
    private ProductDetails productDetails;
    private ProductDetailsResponse productDetailsResponseObj = null;

    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;
    String password;
    @BindView(R.id.im_add_cart)
    ImageView imCart;
    String productId = "";
    String amount = "", rate = "";
    ImageView imMenu, imAppLogo;
    private int flag = 0;
    private List<Images> mProfileImages = new ArrayList<>();
    @BindView(R.id.im_send_quote)
    ImageView imSendQuote;
    private String userReviewFlag = "false";
    String userImages;
    @BindView(R.id.txt_long_description)
    TextView txtLongDescription;
    List<Banner> banners = new ArrayList<>();

    @BindView(R.id.iv_product_logo)
    ImageView imProductLogo;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);

        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  onBackPressed();
                if (flag == 0) {
                    Intent intent = new Intent(ProductDetailsActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(ProductDetailsActivity.this, RelatedProductActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        txtTitle.setText("Product Details");

        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId = sp.getString("customerId", "");
        final Boolean loginStatus = sp.getBoolean("loginStatus", false);
        //  Log.d("_loginStatus", loginStatus.toString());
        productId = sp.getString("productId", "");

        if (getIntent() != null) {
            flag = getIntent().getIntExtra("flag", 0);
        }
        /*if (!"".equalsIgnoreCase(productId)) {
            getProductDetails();
        }*/

        if (TextUtils.isEmpty(customerId)) {
            getProductDetails(productId, "");
        } else {
            getProductDetails(productId, customerId);
        }


        llAboutProduct.setOnClickListener(this);
        llCustomerReviews.setOnClickListener(this);
        llRelatedProducts.setOnClickListener(this);
        imAboutProduct.setOnClickListener(this);
        imCustomerReviews.setOnClickListener(this);
        imRelatedProducts.setOnClickListener(this);
        imCart.setOnClickListener(this);
        imAboutProduct.setOnClickListener(this);
        imAddProduct.setOnClickListener(this);
        imRemoveProduct.setOnClickListener(this);

        imSendQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog();

            }
        });

        imCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (totalQuantity == 0) {
                    Toast.makeText(ProductDetailsActivity.this, "Minimum order for this product is 0", Toast.LENGTH_SHORT).show();
                } else {
                    // Log.d("pd_loginStatus", loginStatus.toString());
                    if (loginStatus == false) {
                        Intent intent = new Intent(ProductDetailsActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        addCart();
                    }
                }
            }
        });
    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductDetailsActivity.this);
        alertDialog.setTitle("Send Quote");
        // alertDialog.setMessage("Enter Password");

        final EditText input = new EditText(ProductDetailsActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        //alertDialog.setIcon(R.drawable.key);
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        password = input.getText().toString();

                        if (TextUtils.isEmpty(password)) {
                            Toast.makeText(ProductDetailsActivity.this, "Please enter message", Toast.LENGTH_SHORT).show();
                            input.setError("please enter message");
                            input.requestFocus();
                            // Log.e("password", password);
                            // sendQuote();
                        } else {
                            // Log.e("password", password);
                            sendQuote();
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void sendQuote() {
        String customerId = "0";

        if (sp.getBoolean("loginStatus", false)) {
            customerId = sp.getString("customerId", "");
        } else {
            customerId = "0";
        }
        showProgress();
        Call<SucessReponse> sendQuote = RestApi.get().getRestService().sendQuote(customerId, productId, password);
        sendQuote.enqueue(new Callback<SucessReponse>() {
            @Override
            public void onResponse(Call<SucessReponse> call, Response<SucessReponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getResponse())) {
                        new AlertDialog.Builder(ProductDetailsActivity.this)
                                .setTitle("Ecom")
                                .setMessage(
                                        response.body().getMessage())
                                .setIcon(
                                        getResources().getDrawable(
                                                android.R.drawable.ic_dialog_alert))
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onFailure(Call<SucessReponse> call, Throwable t) {

            }
        });
    }

    private void getProductDetails(String productId1, String customerId) {

        showProgress();
        txtQuantity.setText("" + totalQuantity);
        Call<ProductDetailsResponse> productDetailsResponseCall = RestApi.get().getRestService().getProductDetails(productId1, customerId);
        productDetailsResponseCall.enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        llProductDetails.setVisibility(View.VISIBLE);
                        txtEmpty.setVisibility(View.GONE);
                         Log.e("productResponse", response.body().getTopRelatedProducts().toString());
                        // txtProductTitle.setText(response.body().getProductDetails().get(0).getA());
                        ProductDetails productDetailsResponse = response.body().getProductDetails();
                        productDetailsResponseObj = response.body();
                        // productDetails = response.body().getProductDetails().getImages();
                        // userImages = new Gson().toJson(response.body().getProductDetails().getImages());
                        if(response.body().getProductDetails().getImages()!=null && response.body().getProductDetails().getImages().size()>0){
                            Picasso.with(getBaseContext()).load(response.body().getProductDetails().getImages().get(0).getImage()).into(imProductLogo);
                        }
                        // Picasso.with(getBaseContext()).load(R.drawable.capture).into(imProductLogo);

                        /*if (response.body().getProductDetails().getImages() != null) {
                            if (response.body().getProductDetails().getImages().size() > 0) {
                                for (int i = 0; i < response.body().getProductDetails().getImages().size(); i++) {
                                    //add banner using image url
                                    banners.add(new RemoteBanner(response.body().getProductDetails().getImages().get(i).getImage()));
//                                    productImagesBanner.setImageURI(null);
//                                    Uri imgUri=Uri.parse(response.body().getProductDetails().getImages().get(i).getImage());
//                                    productImagesBanner.setImageURI(imgUri);

                                }
                                productImagesBanner.setBanners(banners);


                            } else {
                                productImagesBanner.setVisibility(View.GONE);
                            }
                        } else {
                            productImagesBanner.setVisibility(View.GONE);
                        }*/

                       /* productImagesBanner.setBanners(banners);
                        productImagesBanner.setOnBannerClickListener(new OnBannerClickListener() {
                            @Override
                            public void onClick(int position) {
                                Log.d("imagesList1_", userImages);
                                Toast.makeText(ProductDetailsActivity.this, "slder clicked", Toast.LENGTH_SHORT);
                                if (!userImages.isEmpty() || userImages != null) {
                                    try {
                                        Bundle args = new Bundle();
                                        args.putString("userImages", userImages);
                                        FragmentManager manager = getSupportFragmentManager();
                                        ProfilePhotosDialogFragment fragment = new ProfilePhotosDialogFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("userImages", userImages);
                                        bundle.putString("currentUserPosition", mProfileImages.get(position).getId());
                                        fragment.setArguments(bundle);
                                        // alert.setListener(AppointmentFragment.this);
                                        //fragment.setArguments(args);
                                        fragment.show(manager, "UserImages");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        });*/
                        txtProdcutPrice.setText(getResources().getString(R.string.Rs) + " " + response.body().getProductDetails().getRate());
                        if (response.body().getProductDetails().getTitle() != null) {
                            txtProductTitle.setText(response.body().getProductDetails().getTitle());
                        }

                        if (!TextUtils.isEmpty(response.body().getProductDetails().getShortDescription())) {
                            txtProductDesc.setText(response.body().getProductDetails().getShortDescription());
                        }
                        if (!TextUtils.isEmpty(response.body().getProductDetails().getLongDescription())) {
                            String logDesc = productDetailsResponse.getLongDescription();
                            txtLongDescription.setText(Html.fromHtml(logDesc));
                            //  Log.d("longdescription", "string ;;;" + logDesc + "textView;;;" + txtLongDescription.getText().toString());
                        }

                        if (!TextUtils.isEmpty(productDetailsResponse.getRate())) {
                            rate = response.body().getProductDetails().getRate();
                            amount = String.valueOf((int) Float.parseFloat(rate) * totalQuantity);
                        }

                        if (!TextUtils.isEmpty(productDetailsResponse.getProduct_id())) {
                            productId = response.body().getProductDetails().getProduct_id();
                        }

                        if (!TextUtils.isEmpty(productDetailsResponse.getTotalstock())) {
                            orderQtyLimit = (int) Float.parseFloat(response.body().getProductDetails().getTotalstock());
                        }

                        if (!TextUtils.isEmpty(productDetailsResponse.getDeliverycharges())) {
                            txt_delivery_charge_price.setText(response.body().getProductDetails().getDeliverycharges());
                        }

                        if (!TextUtils.isEmpty(productDetailsResponse.getMrp())) {
                            txtProductMrp.setText(getResources().getString(R.string.Rs) + " " + response.body().getProductDetails().getMrp());
                        }

                        if (!TextUtils.isEmpty(productDetailsResponse.getMinimumOrderQty())) {
                            String minOrderStr = "Minimum Order for this product is " + response.body().getProductDetails().getMinimumOrderQty() + " pcs";
                            txt_min_order.setText(minOrderStr);
                            orderQtyLimit = Integer.parseInt(response.body().getProductDetails().getMinimumOrderQty().toString());
                            if (response.body().getProductDetails().getMinimumOrderQty().equalsIgnoreCase("0")) {
                                //  Log.e("min order ", response.body().getProductDetails().getMinimumOrderQty());
                                totalQuantity = 0;
                            }
                        }
                        if (!TextUtils.isEmpty(productDetailsResponse.getLongDescription())) {
                            //  Log.e("long description", productDetailsResponse.getLongDescription());
                            txtLongDescription.setText(Html.fromHtml(productDetailsResponse.getLongDescription()));
                        } else {
                            //txtLongDescription.setText("");
                        }
                        // txtLongDescription.setText(Html.fromHtml(productDetailsResponse.getProductDetails().getLongDescription()));

                        // txt_product_saveed_price.setText(getResources().getString(R.string.Rs) + " ");
                        txt_product_saveed_price.setText(" ");


                        txtQuantity.setText("" + totalQuantity);

                        if (!TextUtils.isEmpty(response.body().getProductDetails().getUserReview())) {
                            userReviewFlag = response.body().getProductDetails().getUserReview();
                        }
                        //  Log.e("userReview", userReviewFlag);
                    } else {
                        llProductDetails.setVisibility(View.VISIBLE);
                        txtEmpty.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                dismissDialog();
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (flag == 0) {
            Intent intent = new Intent(ProductDetailsActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(ProductDetailsActivity.this, RelatedProductActivity.class);
            startActivity(intent);
            //finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_about_product:
                navigateToAboutProduct();
                break;

            case R.id.im_about_product:
                navigateToAboutProduct();
                break;

            case R.id.ll_customer_reviews:
                navigateToCustomerReviews();
                break;

            case R.id.im_customer_reviews:
                navigateToCustomerReviews();
                break;

            case R.id.ll_related_products:
                navigateToRelatedProducts();
                break;

            case R.id.im_related_products:
                navigateToRelatedProducts();
                break;

            case R.id.im_add_product:
                if (totalQuantity != orderQtyLimit) {
                    if (orderQtyLimit != 0) {
                        if (totalQuantity != orderQtyLimit) {
                            totalQuantity = totalQuantity + 1;
                            amount = String.valueOf((int) Float.parseFloat(rate) * totalQuantity);
                            txtQuantity.setText(String.valueOf(totalQuantity));
                        } else {
                            totalQuantity = orderQtyLimit;
                            amount = String.valueOf((int) Float.parseFloat(rate) * totalQuantity);
                            txtQuantity.setText(String.valueOf(totalQuantity));
                        }
                    } else {
                        totalQuantity = totalQuantity + 1;
                        // amount = String.valueOf((int) Float.parseFloat(rate) * totalQuantity);
                        txtQuantity.setText(String.valueOf(totalQuantity));
                    }
                } else {
                    Toast.makeText(ProductDetailsActivity.this, "Reached Maximum Limit", Toast.LENGTH_SHORT).show();

                }

                // Log.e("add", "AddProduct" + amount + "qty" + totalQuantity);
                break;

            case R.id.im_remove_product:
                if (totalQuantity == 1) {
                    totalQuantity = 1;
                    // amount = String.valueOf(Integer.parseInt(rate) * totalQuantity);
                    txtQuantity.setText(String.valueOf(totalQuantity));
                } else {
                    totalQuantity = totalQuantity - 1;
                    // amount = String.valueOf(Integer.parseInt(rate) * totalQuantity);
                    txtQuantity.setText(String.valueOf(totalQuantity));
                }


                break;

            default:
                break;
        }
    }

    private void addCart() {
        int _rate = (int) Float.parseFloat(rate);
        // Log.e("total product qty", totalQuantity + "");
        //Log.e(" product rate", _rate + "");
        amount = String.valueOf(_rate * totalQuantity);
        // Log.e("addCartParms__", customerId + ";;;" + productId + ";;;" + totalQuantity);
        showProgress();
        Call<AddCartResponse> addCartResponseCall = RestApi.get().getRestService().addCart(customerId,
                productId,
                String.valueOf(totalQuantity));

        addCartResponseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        Intent intent = new Intent(ProductDetailsActivity.this, CartActivity.class);
                        intent.putExtra("flag", "1");
                        startActivity(intent);
                        finish();
                        // Log.e("succesResponse", response.body().getMessage());
                    } else {

                        Toast.makeText(ProductDetailsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                dismissDialog();
            }
        });


        /*

         */
    }

    private void navigateToRelatedProducts() {
        Log.e("toralated data", productDetailsResponseObj.getTopRelatedProducts().toString());
        try {
            if (productDetailsResponseObj.getTopRelatedProducts().size() > 0) {

                //String relatedProducts = new Gson().toJson(productDetailsResponseObj);
                String relatedProducts = new Gson().toJson(productDetailsResponseObj.getTopRelatedProducts());
                sp.edit().putString("relatedProducts", relatedProducts).commit();
                Intent intent = new Intent(ProductDetailsActivity.this, RelatedProductActivity.class);
                // intent.putExtra("relatedProducts", relatedProducts);
                intent.putExtra("flag", 0);
                startActivity(intent);
                finish();

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void navigateToCustomerReviews() {
        Intent intent = new Intent(ProductDetailsActivity.this, CustomerReviewsActivity.class);
        intent.putExtra("productId", productId);
        intent.putExtra("userReviewFlag", userReviewFlag);
        startActivity(intent);
        finish();
    }

    private void navigateToAboutProduct() {
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }*/
}

