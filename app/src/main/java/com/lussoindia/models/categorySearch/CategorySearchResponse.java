package com.lussoindia.models.categorySearch;

import java.util.List;

/**
 * Created by Thriveni on 5/18/2018.
 */

public class CategorySearchResponse {
    private String response;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    private String success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    private List<CategoryProducts> categoryProducts;

    public String getResponse ()
    {
        return response;
    }

    public void setResponse (String response)
    {
        this.response = response;
    }

    public List<CategoryProducts> getCategoryProducts ()
    {
        return categoryProducts;
    }

    public void setCategoryProducts (List<CategoryProducts> categoryProducts)
    {
        this.categoryProducts = categoryProducts;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [response = "+response+", categoryProducts = "+categoryProducts+"]";
    }
}
