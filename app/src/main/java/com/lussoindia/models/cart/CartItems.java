package com.lussoindia.models.cart;

import java.util.List;

public class CartItems {
    private String igstAmt;

    private String sgstPercent;

    private String cgstAmt;

    private String reverseChargePercent;

    private String qty;

    private String totalAmt;

    private String reverseChargeAmt;

    private String sgstAmt;

    private String discount;

    private String productId;

    private String id;

    private String stock;

    private String rate;

    private String totTaxAmt;

    private String cgstPercent;

    private String createdAt;

    private String userId;

    private String name;

    private List<Images> images;

    private String totTaxPercent;

    private String igstPercent;

    public String getIgstAmt ()
    {
        return igstAmt;
    }

    public void setIgstAmt (String igstAmt)
    {
        this.igstAmt = igstAmt;
    }

    public String getSgstPercent ()
    {
        return sgstPercent;
    }

    public void setSgstPercent (String sgstPercent)
    {
        this.sgstPercent = sgstPercent;
    }

    public String getCgstAmt ()
    {
        return cgstAmt;
    }

    public void setCgstAmt (String cgstAmt)
    {
        this.cgstAmt = cgstAmt;
    }

    public String getReverseChargePercent ()
    {
        return reverseChargePercent;
    }

    public void setReverseChargePercent (String reverseChargePercent)
    {
        this.reverseChargePercent = reverseChargePercent;
    }

    public String getQty ()
    {
        return qty;
    }

    public void setQty (String qty)
    {
        this.qty = qty;
    }

    public String getTotalAmt ()
    {
        return totalAmt;
    }

    public void setTotalAmt (String totalAmt)
    {
        this.totalAmt = totalAmt;
    }

    public String getReverseChargeAmt ()
    {
        return reverseChargeAmt;
    }

    public void setReverseChargeAmt (String reverseChargeAmt)
    {
        this.reverseChargeAmt = reverseChargeAmt;
    }

    public String getSgstAmt ()
    {
        return sgstAmt;
    }

    public void setSgstAmt (String sgstAmt)
    {
        this.sgstAmt = sgstAmt;
    }

    public String getDiscount ()
    {
        return discount;
    }

    public void setDiscount (String discount)
    {
        this.discount = discount;
    }

    public String getProductId ()
    {
        return productId;
    }

    public void setProductId (String productId)
    {
        this.productId = productId;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getStock ()
    {
        return stock;
    }

    public void setStock (String stock)
    {
        this.stock = stock;
    }

    public String getRate ()
    {
        return rate;
    }

    public void setRate (String rate)
    {
        this.rate = rate;
    }

    public String getTotTaxAmt ()
    {
        return totTaxAmt;
    }

    public void setTotTaxAmt (String totTaxAmt)
    {
        this.totTaxAmt = totTaxAmt;
    }

    public String getCgstPercent ()
    {
        return cgstPercent;
    }

    public void setCgstPercent (String cgstPercent)
    {
        this.cgstPercent = cgstPercent;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public List<Images> getImages ()
    {
        return images;
    }

    public void setImages (List<Images> images)
    {
        this.images = images;
    }

    public String getTotTaxPercent ()
    {
        return totTaxPercent;
    }

    public void setTotTaxPercent (String totTaxPercent)
    {
        this.totTaxPercent = totTaxPercent;
    }

    public String getIgstPercent ()
    {
        return igstPercent;
    }

    public void setIgstPercent (String igstPercent)
    {
        this.igstPercent = igstPercent;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [igstAmt = "+igstAmt+", sgstPercent = "+sgstPercent+", cgstAmt = "+cgstAmt+", reverseChargePercent = "+reverseChargePercent+", qty = "+qty+", totalAmt = "+totalAmt+", reverseChargeAmt = "+reverseChargeAmt+", sgstAmt = "+sgstAmt+", discount = "+discount+", productId = "+productId+", id = "+id+", stock = "+stock+", rate = "+rate+", totTaxAmt = "+totTaxAmt+", cgstPercent = "+cgstPercent+", createdAt = "+createdAt+", userId = "+userId+", name = "+name+", images = "+images+", totTaxPercent = "+totTaxPercent+", igstPercent = "+igstPercent+"]";
    }
}
