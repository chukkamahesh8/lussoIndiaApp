package com.lussoindia.models.productDetails;

import java.util.List;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class ProductDetails {
    private String totalstock;

    private String product_id;

    private String reverseChargeAmount;

    private String mrp;

    private String reverseChargePercent;

    private String date;

    private String brandId;

    private String sgstAmount;

    private String hsncode;

    private String companyid;

    private String title;

    private String saccode;

    private String rate;

    private String ismachine;

    private String longDescription;

    private String userReview;

    private String keywords;

    private String sgstPercent;

    private String category_id;

    private String askForPrice;

    private String cgstAmount;

    private String product_reorder;

    private String minimumOrderQty;

    private String deleted;

    private String unit;

    private String tax;

    private String shortDescription;

    private String cgstPercent;

    private String deliverycharges;

    private String reviewCount;

    private String igstAmount;

    private String mainCatId;

    private List<Images> images;

    private String rating;

    private String igstPercent;

    public String getTotalstock ()
    {
        return totalstock;
    }

    public void setTotalstock (String totalstock)
    {
        this.totalstock = totalstock;
    }

    public String getProduct_id ()
    {
        return product_id;
    }

    public void setProduct_id (String product_id)
    {
        this.product_id = product_id;
    }

    public String getReverseChargeAmount ()
    {
        return reverseChargeAmount;
    }

    public void setReverseChargeAmount (String reverseChargeAmount)
    {
        this.reverseChargeAmount = reverseChargeAmount;
    }

    public String getMrp ()
    {
        return mrp;
    }

    public void setMrp (String mrp)
    {
        this.mrp = mrp;
    }

    public String getReverseChargePercent ()
    {
        return reverseChargePercent;
    }

    public void setReverseChargePercent (String reverseChargePercent)
    {
        this.reverseChargePercent = reverseChargePercent;
    }

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getBrandId ()
    {
        return brandId;
    }

    public void setBrandId (String brandId)
    {
        this.brandId = brandId;
    }

    public String getSgstAmount ()
    {
        return sgstAmount;
    }

    public void setSgstAmount (String sgstAmount)
    {
        this.sgstAmount = sgstAmount;
    }

    public String getHsncode ()
    {
        return hsncode;
    }

    public void setHsncode (String hsncode)
    {
        this.hsncode = hsncode;
    }

    public String getCompanyid ()
    {
        return companyid;
    }

    public void setCompanyid (String companyid)
    {
        this.companyid = companyid;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getSaccode ()
    {
        return saccode;
    }

    public void setSaccode (String saccode)
    {
        this.saccode = saccode;
    }

    public String getRate ()
    {
        return rate;
    }

    public void setRate (String rate)
    {
        this.rate = rate;
    }

    public String getIsmachine ()
    {
        return ismachine;
    }

    public void setIsmachine (String ismachine)
    {
        this.ismachine = ismachine;
    }

    public String getLongDescription ()
    {
        return longDescription;
    }

    public void setLongDescription (String longDescription)
    {
        this.longDescription = longDescription;
    }

    public String getUserReview ()
    {
        return userReview;
    }

    public void setUserReview (String userReview)
    {
        this.userReview = userReview;
    }

    public String getKeywords ()
    {
        return keywords;
    }

    public void setKeywords (String keywords)
    {
        this.keywords = keywords;
    }

    public String getSgstPercent ()
    {
        return sgstPercent;
    }

    public void setSgstPercent (String sgstPercent)
    {
        this.sgstPercent = sgstPercent;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }

    public String getAskForPrice ()
    {
        return askForPrice;
    }

    public void setAskForPrice (String askForPrice)
    {
        this.askForPrice = askForPrice;
    }

    public String getCgstAmount ()
    {
        return cgstAmount;
    }

    public void setCgstAmount (String cgstAmount)
    {
        this.cgstAmount = cgstAmount;
    }

    public String getProduct_reorder ()
    {
        return product_reorder;
    }

    public void setProduct_reorder (String product_reorder)
    {
        this.product_reorder = product_reorder;
    }

    public String getMinimumOrderQty ()
    {
        return minimumOrderQty;
    }

    public void setMinimumOrderQty (String minimumOrderQty)
    {
        this.minimumOrderQty = minimumOrderQty;
    }

    public String getDeleted ()
    {
        return deleted;
    }

    public void setDeleted (String deleted)
    {
        this.deleted = deleted;
    }

    public String getUnit ()
    {
        return unit;
    }

    public void setUnit (String unit)
    {
        this.unit = unit;
    }

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

    public String getShortDescription ()
    {
        return shortDescription;
    }

    public void setShortDescription (String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    public String getCgstPercent ()
    {
        return cgstPercent;
    }

    public void setCgstPercent (String cgstPercent)
    {
        this.cgstPercent = cgstPercent;
    }

    public String getDeliverycharges ()
    {
        return deliverycharges;
    }

    public void setDeliverycharges (String deliverycharges)
    {
        this.deliverycharges = deliverycharges;
    }

    public String getReviewCount ()
    {
        return reviewCount;
    }

    public void setReviewCount (String reviewCount)
    {
        this.reviewCount = reviewCount;
    }

    public String getIgstAmount ()
    {
        return igstAmount;
    }

    public void setIgstAmount (String igstAmount)
    {
        this.igstAmount = igstAmount;
    }

    public String getMainCatId ()
    {
        return mainCatId;
    }

    public void setMainCatId (String mainCatId)
    {
        this.mainCatId = mainCatId;
    }

    public List<Images> getImages ()
    {
        return images;
    }

    public void setImages (List<Images> images)
    {
        this.images = images;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    public String getIgstPercent ()
    {
        return igstPercent;
    }

    public void setIgstPercent (String igstPercent)
    {
        this.igstPercent = igstPercent;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [totalstock = "+totalstock+", product_id = "+product_id+", reverseChargeAmount = "+reverseChargeAmount+", mrp = "+mrp+", reverseChargePercent = "+reverseChargePercent+", date = "+date+", brandId = "+brandId+", sgstAmount = "+sgstAmount+", hsncode = "+hsncode+", companyid = "+companyid+", title = "+title+", saccode = "+saccode+", rate = "+rate+", ismachine = "+ismachine+", longDescription = "+longDescription+", userReview = "+userReview+", keywords = "+keywords+", sgstPercent = "+sgstPercent+", category_id = "+category_id+", askForPrice = "+askForPrice+", cgstAmount = "+cgstAmount+", product_reorder = "+product_reorder+", minimumOrderQty = "+minimumOrderQty+", deleted = "+deleted+", unit = "+unit+", tax = "+tax+", shortDescription = "+shortDescription+", cgstPercent = "+cgstPercent+", deliverycharges = "+deliverycharges+", reviewCount = "+reviewCount+", igstAmount = "+igstAmount+", mainCatId = "+mainCatId+", images = "+images+", rating = "+rating+", igstPercent = "+igstPercent+"]";
    }
}
