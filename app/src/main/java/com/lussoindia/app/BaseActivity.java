package com.lussoindia.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.lussoindia.R;
import com.lussoindia.Utils.Helper;

import java.util.List;


/**
 * Created by Mahesh on 12/27/2017.
 */

public class BaseActivity extends AppCompatActivity {
    SharedPreferences sp;
    private ProgressDialog progressDialog;

    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }
        progressDialog.setMessage("Please wait..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences("SheCabs", Context.MODE_PRIVATE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadFragment(int fragmentAddOrReplace, Fragment fragmentToLoad, String fragmentTag, Bundle mBundleData, boolean hasBackStack) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);

            //fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, /*R.anim.exit_to_left,*/ R.anim.enter_from_left/*, R.anim.exit_to_right*/);

            //remove all fragment when click on home fragment
           /* if (fragmentToLoad instanceof BusinessFragment) {
                fragmentManager.popBackStack(fragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }*/

//            List<Fragment> allFragments = fragmentManager.getFragments();
//            if (allFragments == null || allFragments.isEmpty()) {
//                fragmentAddOrReplace = Helper.FRAGMENT_ADD;
//            }

            if (mBundleData != null) {
                fragmentToLoad.setArguments(mBundleData);
            }

//            if (this instanceof MainDashboardActivity) {

            Fragment visibleFragment = getVisibleFragment();
            if (visibleFragment != null && visibleFragment.getClass().getName().equalsIgnoreCase(fragmentTag)) {
                return;
            }
//            }

            if (fragmentAddOrReplace == Helper.FRAGMENT_ADD) {
               // fragmentTransaction.add(R.id.frame, fragmentToLoad, fragmentTag);
            } else if (fragmentAddOrReplace == Helper.FRAGMENT_REPLACE) {
//                if (fragmentToLoad.isAdded()) {
//                    fragmentTransaction.show(fragmentToLoad);
//                } else {
//                for (int i = 0; i < fragmentManager.getBackStackEntryCount()-1; i++) {
//                    fragmentManager.popBackStack();
//                }
               // fragmentTransaction.replace(R.id.frame, fragmentToLoad, fragmentTag);
                //  }
            }
            if (hasBackStack) {
                fragmentTransaction.addToBackStack(fragmentTag);
            } else {
                fragmentTransaction.addToBackStack(null);
                fragmentManager.popBackStack(fragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            fragmentTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments == null) {
            return null;
        }

        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }

}
