package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.holders.HomeProductsHolder;
import com.lussoindia.models.home.Products;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SeeMoreProducts extends BaseActivity {
    @BindView(R.id.rv_see_more_products)
    RecyclerView rvRelatedProducts;
    private SharedPreferences sp = null;
    Snackbar snackbar;
    private String customerId = "", adapterPosition;
    @BindView(R.id.txt_empty)
    TextView txtEmpty;
    int flag = 1;
    RelatedProductsAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more_products);
        ButterKnife.bind(this);
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId = sp.getString("customerId", "");//
        if (getIntent() != null) {
            adapterPosition = getIntent().getStringExtra("adapterPosition");
        }
        Type type = new TypeToken<List<Products>>() {
        }.getType();
        if (!TextUtils.isEmpty(sp.getString("relatedProducts", ""))) {
           // Log.e("RelatedProduct", "From Product details");
            List<Products> topRelatedProductsList = new Gson().fromJson(sp.getString("relatedProducts", ""), type);
            // List<TopRelatedProducts> topRelatedProductsList = sp.getString("relatedProducts",""));//new Gson().fromJson(getIntent().getStringExtra("relatedProducts"), type);
            txtEmpty.setVisibility(View.GONE);
            rvRelatedProducts.setVisibility(View.VISIBLE);
            if (topRelatedProductsList != null && topRelatedProductsList.size() > 0) {
                adapter.addItems(topRelatedProductsList);
                rvRelatedProducts.setAdapter(adapter);
            }
        }
    }

    class RelatedProductsAdapter extends RecyclerView.Adapter<HomeProductsHolder> {
        public Context context;
        public List<Products> topRelatedProducts = new ArrayList<>();

        public RelatedProductsAdapter(Context context) {
            this.context = context;
        }

        public void addItems(List<Products> topRelatedProducts) {
            this.topRelatedProducts = topRelatedProducts;
        }

        @Override
        public HomeProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_products, parent, false);

            return new HomeProductsHolder(itemView);
        }

        @Override
        public void onBindViewHolder(HomeProductsHolder holder, final int position) {
            holder.getTxtProductTitle().setText(topRelatedProducts.get(position).getTitle());
            holder.getTxtProductDesc().setText(topRelatedProducts.get(position).getShortDescription());
            holder.getGetTxtProductMrp().setText(topRelatedProducts.get(position).getMrp());
            Picasso.with(context).load(topRelatedProducts.get(position).getImages().get(0).getImage())
                    .into(holder.getImProduct());
            holder.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(SeeMoreProducts.this, "You clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    sp.edit().putString("productId", topRelatedProducts.get(position).getProduct_id()).commit();
                    // intent.putExtra("productId", topRelatedProducts.get(position).getProduct_id());
                    intent.putExtra("flag", 1);
                    startActivity(intent);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return topRelatedProducts.size();
        }
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        super.onBackPressed();
    }
}
