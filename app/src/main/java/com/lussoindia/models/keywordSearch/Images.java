package com.lussoindia.models.keywordSearch;

/**
 * Created by Thriveni on 5/15/2018.
 */

public class Images {
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ClassPojo [image = " + image + "]";
    }
}
