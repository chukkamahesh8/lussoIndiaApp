package com.lussoindia.models.home;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class Banners {
    private String updatedAt;

    private String id;

    private String startDate;

    private String createdAt;

    private String link;

    private String name;

    private String deletedAt;

    private String image;

    private String endDate;

    private String type;

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ClassPojo [updatedAt = " + updatedAt + ", id = " + id + ", startDate = " + startDate + ", createdAt = " + createdAt + ", link = " + link + ", name = " + name + ", deletedAt = " + deletedAt + ", image = " + image + ", endDate = " + endDate + ", type = " + type + "]";
    }
}
