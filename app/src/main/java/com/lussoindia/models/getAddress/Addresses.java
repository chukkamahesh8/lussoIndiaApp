package com.lussoindia.models.getAddress;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class Addresses {
    private String city_id;

    private String id;

    private String zip;

    private String addr_line2;

    private String taluka;

    private String name;

    private String state;

    private String addr_line1;

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddr_line2() {
        return addr_line2;
    }

    public void setAddr_line2(String addr_line2) {
        this.addr_line2 = addr_line2;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddr_line1() {
        return addr_line1;
    }

    public void setAddr_line1(String addr_line1) {
        this.addr_line1 = addr_line1;
    }

    @Override
    public String toString() {
        return "ClassPojo [city_id = " + city_id + ", id = " + id + ", zip = " + zip + ", addr_line2 = " + addr_line2 + ", taluka = " + taluka + ", name = " + name + ", state = " + state + ", addr_line1 = " + addr_line1 + "]";
    }
}
