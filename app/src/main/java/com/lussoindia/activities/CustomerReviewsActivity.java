package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.holders.CustomerReviewHolder;
import com.lussoindia.models.addProductReview.AddProductReviewResponse;
import com.lussoindia.models.prodcutReview.ProductReview;
import com.lussoindia.models.prodcutReview.Reviews;
import com.lussoindia.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 5/8/2018.
 */

public class CustomerReviewsActivity extends BaseActivity {
    @BindView(R.id.rv_customer_reviews)
    RecyclerView rvCustomerReviews;

    @BindView(R.id.et_review)
    EditText etReview;

    @BindView(R.id.btn_review_submit)
    Button btnReviewSubmit;

    @BindView(R.id.rating_bar)
    RatingBar customerReviewRating;

    private String productId = "", customerId = "";

    private SharedPreferences sp = null;
    CustomerReviewAdapter adapter;

    float ratingVal = 5;
    android.support.v7.widget.Toolbar toolbar;
    TextView txtPageTitle;
    ImageView imMenu, imAppLogo;
    private String userReviewFlag;

    @BindView(R.id.ll_customer_add_review_layout)
    LinearLayout llCustomerAddReviewlayout;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.im_back)
    ImageView imBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_review);
        ButterKnife.bind(this);
        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        txtTitle.setText("Customer Reviews");
       /* toolbar = findViewById(R.id.toolbar);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("Customer Reviews");
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setVisibility(View.GONE);
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo);
        imAppLogo.setVisibility(View.GONE);
        ImageView imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        toolbar.getNavigationIcon().setColorFilter(ContextCompat.getColor(CustomerReviewsActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
*/
        adapter = new CustomerReviewAdapter(CustomerReviewsActivity.this);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCustomerReviews.setLayoutManager(layoutManager);
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId = sp.getString("customerId", "");

        productId = sp.getString("productId", "");
        if (getIntent() != null) {
            userReviewFlag = getIntent().getStringExtra("userReviewFlag");
            Log.e("userReview", userReviewFlag);
            if ((TextUtils.isEmpty(userReviewFlag)) || ("true".equalsIgnoreCase(userReviewFlag))) {
                llCustomerAddReviewlayout.setVisibility(View.GONE);
            } else if (TextUtils.isEmpty(customerId)) {
                llCustomerAddReviewlayout.setVisibility(View.GONE);
            } else {
                llCustomerAddReviewlayout.setVisibility(View.VISIBLE);
            }

        }


        if (!"".equalsIgnoreCase(productId)) {
            getCustomerRating(productId);
        }
        // customerReviewRating.setRating(2);

        customerReviewRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingVal = rating;
                customerReviewRating.setRating(ratingVal);
                // Toast.makeText(CustomerReviewsActivity.this, "Click on rating" + ratingVal, Toast.LENGTH_SHORT).show();
            }
        });

        btnReviewSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etReview.getText().toString().trim())) {
                    etReview.setError("Please enter review");
                    etReview.requestFocus();
                } else if (ratingVal == 0) {
                    Toast.makeText(CustomerReviewsActivity.this, "Please pick rating", Toast.LENGTH_SHORT).show();
                } else {
                    showProgress();
                    Call<AddProductReviewResponse> addProductReview = RestApi.get().getRestService().addProductReviewResponse(productId, customerId, String.valueOf(ratingVal), etReview.getText().toString().trim());
                    addProductReview.enqueue(new Callback<AddProductReviewResponse>() {
                        @Override
                        public void onResponse(Call<AddProductReviewResponse> call, Response<AddProductReviewResponse> response) {
                            dismissDialog();
                            if (response.isSuccessful()) {
                                if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                                    etReview.setText("");
                                    customerReviewRating.setRating(0);
                                    llCustomerAddReviewlayout.setVisibility(View.GONE);
                                    Toast.makeText(CustomerReviewsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    getCustomerRating(productId);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<AddProductReviewResponse> call, Throwable t) {
                            dismissDialog();
                        }
                    });
                }
            }
        });
    }

    private void getCustomerRating(String productId) {
        showProgress();
        Call<ProductReview> getProductReviews = RestApi.get().getRestService().productReview(productId);
        getProductReviews.enqueue(new Callback<ProductReview>() {
            @Override
            public void onResponse(Call<ProductReview> call, Response<ProductReview> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        adapter.addItems(response.body().getReviews());
                        rvCustomerReviews.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductReview> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    class CustomerReviewAdapter extends RecyclerView.Adapter<CustomerReviewHolder> {
        private Context context;
        private List<Reviews> customerReviewsList = new ArrayList<>();

        public CustomerReviewAdapter(Context context) {
            this.context = context;
        }

        public void addItems(List<Reviews> customerReviewsList) {
            this.customerReviewsList = customerReviewsList;
        }

        @Override
        public CustomerReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_customer_review, parent, false);

            return new CustomerReviewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(CustomerReviewHolder holder, int position) {
            String city, state;
            holder.getTxtCustomerName().setText(customerReviewsList.get(position).getCustomerName());
            holder.getTxtReviewName().setText(customerReviewsList.get(position).getReview());
            holder.getRatingBar().setRating(Float.parseFloat(customerReviewsList.get(position).getRating()));

            if (!TextUtils.isEmpty(customerReviewsList.get(position).getCustomerCity()) && !TextUtils.isEmpty(customerReviewsList.get(position).getCustomerState())) {
                city = customerReviewsList.get(position).getCustomerCity();
                holder.getTxtCity().setText(customerReviewsList.get(position).getCustomerCity() + ", " + customerReviewsList.get(position).getCustomerState());

            } else {
                city = "";
                holder.getTxtCity().setText("");
            }

            if (!TextUtils.isEmpty(customerReviewsList.get(position).getCustomerState())) {
                state = customerReviewsList.get(position).getCustomerState();
            } else {
                state = "";
            }


        }

        @Override
        public int getItemCount() {
            return customerReviewsList.size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, ProductDetailsActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, ProductDetailsActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }
}
