package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lussoindia.R;
import com.lussoindia.adapters.AbstractBaseAdapter;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.fragments.CategoriesDialog;
import com.lussoindia.holders.Home1Holder;
import com.lussoindia.holders.Home1ProductHolder;
import com.lussoindia.models.home.DisplayItems;
import com.lussoindia.models.home.HomeResponse;
import com.lussoindia.models.home.Products;
import com.lussoindia.network.NetworkConnectivity;
import com.lussoindia.network.RestApi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;

/**
 * Created by Thriveni on 6/13/2018.
 */

public class ActivityHome extends BaseActivity implements TextWatcher, View.OnClickListener {
    android.support.v7.widget.Toolbar toolbar;
    TextView txtSearch;
    @BindView(R.id.lv_home_list)
    ListView rvHomeList;
    HomeAdapter adapter;
    private String customerId = "";
    @BindView(R.id.txt_empty)
    TextView txtEmpty;
    Snackbar snackbar;
    @BindView(R.id.parentLayout)
    LinearLayout Parentlayout;
    EditText et_search;
    ImageView imCat, imMenu;
    private SharedPreferences sp = null;
    List<DisplayItems> homeDisplayItemsList = new ArrayList<>();
    TextView txtPageTitle;
    PopupMenu popup = null;
    ImageView imAppLogo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        ButterKnife.bind(this);

        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),
                R.drawable.ic_about);
        // toolbar.setOverflowIcon(drawable);
        customerId = sp.getString("customerId", "");//
        toolbar = findViewById(R.id.toolbar);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setColorFilter(ContextCompat.getColor(ActivityHome.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("Home");
        txtPageTitle.setTextColor(getResources().getColor(R.color.colorAccent));
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo);

        imMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup = new PopupMenu(ActivityHome.this, (View) imMenu);
                popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());
                if (sp.getBoolean("loginStatus", false)) {
                    if (popup.getMenu() != null) {
                        popup.getMenu().findItem(R.id.login).setVisible(false);
                        popup.getMenu().findItem(R.id.logout).setVisible(true);
                        popup.getMenu().findItem(R.id.my_cart).setVisible(true);
                        popup.getMenu().findItem(R.id.my_profile).setVisible(true);
                        popup.getMenu().findItem(R.id.my_orders).setVisible(true);
                    }
                } else {
                    popup.getMenu().findItem(R.id.login).setVisible(true);
                    popup.getMenu().findItem(R.id.logout).setVisible(false);
                    popup.getMenu().findItem(R.id.my_cart).setVisible(false);
                    popup.getMenu().findItem(R.id.my_profile).setVisible(false);
                    popup.getMenu().findItem(R.id.my_orders).setVisible(false);
                }


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = null;
                        switch (item.getItemId()) {

                            case R.id.my_cart:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(ActivityHome.this, CartActivity.class);
                                intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.my_profile:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(ActivityHome.this, MyProfileActivity.class);
                                intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.my_orders:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(ActivityHome.this, MyOrdersActivity.class);
                                // intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.login:
                                // read the listItemPositionForPopupMenu here
                                intent = new Intent(ActivityHome.this, LoginActivity.class);
                                intent.putExtra("flag", "0");
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.logout:
                                sp.edit().putBoolean("loginStatus", false).commit();
                                sp.edit().putString("customerId", "").commit();
                                popup.getMenu().findItem(R.id.login).setVisible(false);
                                popup.getMenu().findItem(R.id.logout).setVisible(true);
                                popup.getMenu().findItem(R.id.my_cart).setVisible(true);
                                popup.getMenu().findItem(R.id.my_profile).setVisible(true);
                                return true;


                            default:
                                return false;
                        }


                    }
                });

                popup.show();
            }
        });
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        et_search = (EditText) findViewById(R.id.et_search);
        txtSearch = (TextView) findViewById(R.id.txt_search);
        //et_search.setText("essse");
        imCat = (ImageView) findViewById(R.id.im_cat);
        imCat.setColorFilter(ContextCompat.getColor(ActivityHome.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        txtSearch.setTextColor(getResources().getColor(R.color.colorAccent));
        txtSearch.setOnClickListener(this);

        //et_search.addTextChangedListener(this);
        adapter = new HomeAdapter(this);

        // customerId = sp.getString("customerId", "");

        imCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getSupportFragmentManager();
                CategoriesDialog fragment = new CategoriesDialog().newInstance("Services");
                //fragment.setListener(CreateEmployeeActivity.this);
                fragment.show(fm, "services_dialog");
            }
        });

        networkCheck();
    }

    private void networkCheck() {
        if (NetworkConnectivity.isAvailable(ActivityHome.this)) {

            if (TextUtils.isEmpty(customerId)) {
                getHomeList("");
            } else {
                getHomeList(customerId);
            }
        } else {
            snackbar = Snackbar
                    .make(Parentlayout, "No Internet Connection", Snackbar.LENGTH_LONG)
                    .setAction("Enable", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(dialogIntent);
                        }
                    });

            snackbar.show();
        }


    }

    private void getHomeList(String customerId) {
        showProgress();
        //Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeList();
        Call<HomeResponse> getHomeList = RestApi.get().getRestService().getHomeList(customerId);
        getHomeList.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                        homeDisplayItemsList = response.body().getDisplayItems();
                        txtEmpty.setVisibility(View.GONE);
                        rvHomeList.setVisibility(View.VISIBLE);
                        adapter.addItems(response.body().getDisplayItems());
                        rvHomeList.setAdapter(adapter);
                    } else {

                        txtEmpty.setVisibility(View.VISIBLE);
                        rvHomeList.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {

    }

    class HomeAdapter extends AbstractBaseAdapter<DisplayItems, Home1Holder> {
        public HomeProductAdapter homeProductAdapter1;
        Context context;

        public HomeAdapter(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        public int getViewTypeCount() {

            return getCount();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public int getLayoutId() {
            return R.layout.home_item;
        }

        @Override
        public Home1Holder getViewHolder(View convertView) {
            return new Home1Holder(convertView);
        }

        @Override
        public void bindView(int position, Home1Holder holder, DisplayItems item) {
            List<Banner> banners = new ArrayList<>();

            if (item.getBanners().size() > 0) {
                for (int i = 0; i < item.getBanners().size(); i++) {
                    //add banner using image url
                    banners.add(new RemoteBanner(item.getBanners().get(i).getImage()));
                }
                holder.getBannerSlider().setBanners(banners);
                holder.getBannerSlider().setOnBannerClickListener(new OnBannerClickListener() {
                    @Override
                    public void onClick(int position) {
                        //Toast.makeText(HomeActivity.this, "Banner with position " + String.valueOf(position) + " clicked!", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                //holder.getBannerSlider().setVisibility(View.GONE);
            }

            holder.getTxtType().setText(item.getType());

            //  Log.e("product type", ":::::" + item.getType());
            /*homeProductAdapter1 = new HomeProductAdapter(context);
            homeProductAdapter1.addItems(item.getProducts());
            holder.getLvProducts().setAdapter(homeProductAdapter1);*/

            if (item.getProducts() != null) {
                Log.e("product type", ":::::" + item.getProducts().size()+";;;;"+item.getProducts().toString());
                if (item.getProducts().size() > 0) {
                    for (int i = 0; i < item.getProducts().size(); i++) {
                        View view = getLayoutInflater().inflate(R.layout.home_product_item, holder.getLlHomeProducts(), false);
                        ImageView imProduct = (ImageView) view.findViewById(R.id.im_product);
                       // Picasso.with(context).load(item.getProducts().get(i).getImages().get(0).getImage()).into(imProduct);
                        TextView txtProductTitle = (TextView) view.findViewById(R.id.txt_product_title);
                        txtProductTitle.setText(item.getProducts().get(i).getTitle());

                        TextView txtProductDescription = (TextView) view.findViewById(R.id.txt_product_desc);
                        txtProductDescription.setText(item.getProducts().get(i).getShortDescription());

                        TextView txtProductMrp = (TextView) view.findViewById(R.id.txt_product_mrp);
                        txtProductMrp.setText(getResources().getString(R.string.Rs)+"  "+item.getProducts().get(i).getMrp());
                        holder.getLlHomeProducts().addView(view);
                    }
                }
            }


        }
    }


    class HomeProductAdapter extends AbstractBaseAdapter<Products, Home1ProductHolder> {
        Context context;

        public HomeProductAdapter(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        public int getLayoutId() {
            return R.layout.home_product_item;
        }

        @Override
        public Home1ProductHolder getViewHolder(View convertView) {
            return new Home1ProductHolder(convertView);
        }

        @Override
        public void bindView(int position, Home1ProductHolder holder, final Products item) {
            Log.e("product title", "--->" + item.getTitle());
            holder.getTxtProductTitle().setText(item.getTitle() + item.getProduct_id());
            holder.getTxtProductDesc().setText(item.getShortDescription());
            holder.getGetTxtProductMrp().setText(item.getMrp());
            Picasso.with(context).load(item.getImages().get(0).getImage())
                    .into(holder.getImProduct());
            holder.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Selected pId::", item.getProduct_id());
                    /*sp.edit().putString("productId", item.getProduct_id()).commit();
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("flag", 0);
                    startActivity(intent);
                    finish();*/
                }
            });
        }
    }
}

