package com.lussoindia.models;

/**
 * Created by Thriveni on 6/4/2018.
 */

public class SucessReponse {
    private String message;

    private String response;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", response = " + response + "]";
    }
}
