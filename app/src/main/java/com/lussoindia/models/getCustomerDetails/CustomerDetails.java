package com.lussoindia.models.getCustomerDetails;

/**
 * Created by Thriveni on 4/30/2018.
 */

public class CustomerDetails {
    private String addr_line2;

    private String city_name;

    private String gstin;

    private String state;

    private String sales_manager;

    private String password;

    private String id;

    private String city_id;

    private String companyid;

    private String taluka;

    private String username;

    private String sales_phone;

    private String createdAt;

    private String name;

    private String customerCode;

    private String phone_no;

    private String addr_line1;

    private String mobile_no;

    private String customerName;

    private String zip;

    private String fnb_manager;

    private String purchase_manager;

    private String fnb_phone;

    private String code;

    private String shippingCharges;

    private String email;

    private String stateId;

    private String purchase_phone;

    private String customer_id;

    public String getAddr_line2() {
        return addr_line2;
    }

    public void setAddr_line2(String addr_line2) {
        this.addr_line2 = addr_line2;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getGstin() {
        return gstin;
    }

    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSales_manager() {
        return sales_manager;
    }

    public void setSales_manager(String sales_manager) {
        this.sales_manager = sales_manager;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSales_phone() {
        return sales_phone;
    }

    public void setSales_phone(String sales_phone) {
        this.sales_phone = sales_phone;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getAddr_line1() {
        return addr_line1;
    }

    public void setAddr_line1(String addr_line1) {
        this.addr_line1 = addr_line1;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getFnb_manager() {
        return fnb_manager;
    }

    public void setFnb_manager(String fnb_manager) {
        this.fnb_manager = fnb_manager;
    }

    public String getPurchase_manager() {
        return purchase_manager;
    }

    public void setPurchase_manager(String purchase_manager) {
        this.purchase_manager = purchase_manager;
    }

    public String getFnb_phone() {
        return fnb_phone;
    }

    public void setFnb_phone(String fnb_phone) {
        this.fnb_phone = fnb_phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(String shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getPurchase_phone() {
        return purchase_phone;
    }

    public void setPurchase_phone(String purchase_phone) {
        this.purchase_phone = purchase_phone;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [addr_line2 = " + addr_line2 + ", city_name = " + city_name + ", gstin = " + gstin + ", state = " + state + ", sales_manager = " + sales_manager + ", password = " + password + ", id = " + id + ", city_id = " + city_id + ", companyid = " + companyid + ", taluka = " + taluka + ", username = " + username + ", sales_phone = " + sales_phone + ", createdAt = " + createdAt + ", name = " + name + ", customerCode = " + customerCode + ", phone_no = " + phone_no + ", addr_line1 = " + addr_line1 + ", mobile_no = " + mobile_no + ", customerName = " + customerName + ", zip = " + zip + ", fnb_manager = " + fnb_manager + ", purchase_manager = " + purchase_manager + ", fnb_phone = " + fnb_phone + ", code = " + code + ", shippingCharges = " + shippingCharges + ", email = " + email + ", stateId = " + stateId + ", purchase_phone = " + purchase_phone + ", customer_id = " + customer_id + "]";
    }
}
