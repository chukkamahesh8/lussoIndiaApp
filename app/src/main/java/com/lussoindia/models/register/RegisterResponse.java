package com.lussoindia.models.register;

/**
 * Created by Thriveni on 5/20/2018.
 */

public class RegisterResponse {
    private String message;

    private String userid;
    private String customerId;

    private String success;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String customerId)
    {
        this.userid = customerId;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", customerId = "+userid+", success = "+success+"]";
    }
}
