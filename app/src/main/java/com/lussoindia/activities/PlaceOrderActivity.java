package com.lussoindia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lussoindia.R;
import com.lussoindia.app.BaseActivity;
import com.lussoindia.holders.CartHolder;
import com.lussoindia.models.cart.CartItems;
import com.lussoindia.models.cart.CustomerDetails;
import com.lussoindia.models.cart.Totals;
import com.lussoindia.models.cart.UserCartResponse;
import com.lussoindia.network.RestApi;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceOrderActivity extends BaseActivity {
    @BindView(R.id.txt_total_discount)
    TextView txtTotalDiscount;
    @BindView(R.id.txt_total_amount)
    TextView txtTotalAmount;
    @BindView(R.id.txt_o_name)
    TextView txtName;
    @BindView(R.id.txt_o_address)
    TextView txtAddress;
    @BindView(R.id.txt_o_phone_no)
    TextView txtPhoneMo;
    @BindView(R.id.txt_o_gst_no)
    TextView txtGstNo;
    ImageView imPlaceOrder;
    @BindView(R.id.rv_cart)
    RecyclerView rvCart;
    SharedPreferences sp;
    PlaceOrderAdapter adapter;
    private String customerId;
    ImageView imMenu, imAppLogo, imCart;
    TextView txtPageTitle;
    android.support.v7.widget.Toolbar toolbar;

    //total
    @BindView(R.id.txt_tb_tax)
    TextView txtTBTax;
    @BindView(R.id.txt_tot_discount)
    TextView txtToTDiscount;
    @BindView(R.id.txt_tot_cgst_percent)
    TextView txtCgstPer;
    @BindView(R.id.txt_tot_cgst_amount)
    TextView txtCgstAmt;
    @BindView(R.id.txt_tot_sgst_amount)
    TextView txtSgstAmt;
    @BindView(R.id.txt_tot_sgst_per)
    TextView txtSgstPer;
    @BindView(R.id.txt_tot_igst_percent)
    TextView txtIgstPer;
    @BindView(R.id.txt_tot_igst_amount)
    TextView txtIgstAmt;
    @BindView(R.id.txt_tot_rc_percent)
    TextView txtRctPer;
    @BindView(R.id.txt_tot_rc_amount)
    TextView txtRcAmt;
    @BindView(R.id.txt_tot_shippingCharges)
    TextView txtShippingCharges;
    @BindView(R.id.txt_tot_grosstotal)
    TextView txtGrossTotl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);
        ButterKnife.bind(this);
        sp = getSharedPreferences("LussoIndia", Context.MODE_PRIVATE);
        customerId = sp.getString("customerId", "");// "670";//
        // Log.d("customerId__", customerId);
        toolbar = findViewById(R.id.toolbar);
        imMenu = (ImageView) toolbar.findViewById(R.id.im_menu);
        imMenu.setVisibility(View.GONE);
        txtPageTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtPageTitle.setText("Order Summery");
        imAppLogo = (ImageView) toolbar.findViewById(R.id.im_app_logo);
        imCart = (ImageView) toolbar.findViewById(R.id.im_cart);
        imPlaceOrder = (ImageView) toolbar.findViewById(R.id.im_place_order);
        imPlaceOrder.setVisibility(View.VISIBLE);
        imAppLogo.setVisibility(View.GONE);
        imCart.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_ic);
        toolbar.getNavigationIcon().setColorFilter(ContextCompat.getColor(PlaceOrderActivity.this, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCart.setLayoutManager(layoutManager);
        adapter = new PlaceOrderAdapter(this);
        getOrderItems();
        imPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlaceOrderActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent;
        homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    private void getOrderItems() {
        showProgress();
        Call<UserCartResponse> userCartResponseCall = RestApi.get().getRestService().getUserCart(customerId);
        userCartResponseCall.enqueue(new Callback<UserCartResponse>() {
            @Override
            public void onResponse(Call<UserCartResponse> call, Response<UserCartResponse> response) {
                dismissDialog();
                CustomerDetails cutomerDetails = response.body().getResponseData().getCustomerDetails();
                if ("true".equalsIgnoreCase(response.body().getSuccess())) {
                    Log.e("cartSucees__", response.body().getSuccess());
                    if (response.body().getResponseData() != null) {
                        adapter.addItems(response.body().getResponseData().getCartItems());
                        txtName.setText(cutomerDetails.getCustomerName());
                        txtGstNo.setText(cutomerDetails.getCustomerGstNo());
                        txtAddress.setText(cutomerDetails.getCustomerAddress());
                        txtPhoneMo.setText(cutomerDetails.getCustomerPhone());
                        rvCart.setAdapter(adapter);
                        rvCart.setVisibility(View.VISIBLE);
                        Totals priceDetails = response.body().getResponseData().getTotals();
                        if (priceDetails.getGrossTotal().equalsIgnoreCase("0")) {
                            txtGrossTotl.setText("GrossTotal " + " : " + priceDetails.getGrossTotal());
                        } else {
                            if (priceDetails.getShippingCharges().equalsIgnoreCase("0")) {
                                txtShippingCharges.setVisibility(View.GONE);
                            } else {
                                txtShippingCharges.setText("ShippingCharges " + " : " + priceDetails.getShippingCharges());
                            }
                            if (priceDetails.getTotalBeforeTax().equalsIgnoreCase("0")) {
                                txtTBTax.setVisibility(View.GONE);
                            } else {
                                txtTBTax.setText("TotalBeforeTax " + " : " + priceDetails.getTotalBeforeTax());
                            }
                            if (priceDetails.getTotalDiscount().equalsIgnoreCase("0")) {
                                txtTotalDiscount.setVisibility(View.GONE);
                            } else {
                                txtTotalDiscount.setText("TotalDiscount " + " : " + priceDetails.getTotalDiscount());
                            }
                            if (priceDetails.getTotalCgstPercent().equalsIgnoreCase("0")) {
                                txtCgstPer.setVisibility(View.GONE);
                            } else {
                                txtCgstPer.setText("TotalCgstPercent " + " : " + priceDetails.getTotalCgstPercent());
                            }
                            if (priceDetails.getTotalCgstAmt().equalsIgnoreCase("0")) {
                                txtCgstAmt.setVisibility(View.GONE);
                            } else {
                                txtCgstAmt.setText("TotalCgstAmt " + " : " + priceDetails.getTotalCgstAmt());
                            }
                            if (priceDetails.getTotalSgstPercent().equalsIgnoreCase("0")) {
                                txtSgstPer.setVisibility(View.GONE);
                            } else {
                                txtSgstPer.setText("TotalSgstPercent " + " : " + priceDetails.getTotalSgstPercent());
                            }
                            if (priceDetails.getTotalSgstAmt().equalsIgnoreCase("0")) {
                                txtSgstAmt.setVisibility(View.GONE);
                            } else {
                                txtSgstAmt.setText("TotalSgstAmt " + " : " + priceDetails.getTotalSgstAmt());
                            }
                            if (priceDetails.getTotalIgstPercent().equalsIgnoreCase("0")) {
                                txtIgstPer.setVisibility(View.GONE);
                            } else {
                                txtIgstPer.setText("TotalIgstPercent " + " : " + priceDetails.getTotalIgstPercent());
                            }
                            if (priceDetails.getTotalIgstAmt().equalsIgnoreCase("0")) {
                                txtIgstAmt.setVisibility(View.GONE);
                            } else {
                                txtIgstAmt.setText("TotalIgstAmt " + " : " + priceDetails.getTotalIgstAmt());
                            }
                            if (priceDetails.getTotalReverseChargeAmt().equalsIgnoreCase("0")) {
                                txtRcAmt.setVisibility(View.GONE);
                            } else {
                                txtRcAmt.setText("TotalReverseChargeAmt " + " : " + priceDetails.getTotalReverseChargeAmt());
                            }
                            if (priceDetails.getTotalReverseChargePercent().equalsIgnoreCase("0")) {
                                txtRctPer.setVisibility(View.GONE);
                            } else {
                                txtRctPer.setText("TotalReverseChargePercent " + " : " + priceDetails.getTotalReverseChargePercent());
                            }
                            if (priceDetails.getGrossTotal().equalsIgnoreCase("0")) {

                            } else {
                                txtGrossTotl.setText("GrossTotal " + " : " + priceDetails.getGrossTotal());
                            }
                        }
                        txtTotalAmount.setText(getResources().getString(R.string.Rs) + " " + priceDetails.getGrossTotal());
                        txtTotalDiscount.setText(getResources().getString(R.string.Rs) + " " + priceDetails.getTotalDiscount());
                    } else {
                        rvCart.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<UserCartResponse> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    class PlaceOrderAdapter extends RecyclerView.Adapter<CartHolder> {
        private Context context;
        private List<CartItems> listCartItems = new ArrayList<>();
        int quantity, rate, totalAmount;
        private String cartItemId, productId;

        public PlaceOrderAdapter(Context context) {
            this.context = context;
        }

        public void addItems(List<CartItems> listCartItems) {
            this.listCartItems = listCartItems;
        }

        @Override
        public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_cart, parent, false);

            return new CartHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final CartHolder holder, final int position) {
            holder.getTxtProductTitle().setText(listCartItems.get(position).getName());
            holder.getAmount().setText(listCartItems.get(position).getRate());
            holder.getTxtAmount().setText(getResources().getString(R.string.Rs) + " " + listCartItems.get(position).getTotalAmt());
            holder.getTxtDate().setText(listCartItems.get(position).getCreatedAt());
            holder.getTxtQuantity().setText(listCartItems.get(position).getQty());
            holder.getLlAddQty().setVisibility(View.GONE);
            holder.getImDeleteItem().setVisibility(View.GONE);
            Picasso.with(context).load(listCartItems.get(position).getImages().get(0).getImage())
                    .into(holder.getImProductImz());
            quantity = Integer.parseInt(listCartItems.get(position).getQty());
            rate = (int) Float.parseFloat(listCartItems.get(position).getRate());
            totalAmount = (int) Float.parseFloat(listCartItems.get(position).getTotalAmt());

          /*  holder.getImProductImz().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sp.edit().putString("productId", listCartItems.get(position).getProductId()).commit();
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("flag", 0);
                    startActivity(intent);
                    finish();
                }
            });*/


        }

        @Override
        public int getItemCount() {
            return listCartItems.size();
        }
    }
}
