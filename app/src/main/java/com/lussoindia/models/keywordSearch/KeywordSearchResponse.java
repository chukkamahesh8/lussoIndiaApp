package com.lussoindia.models.keywordSearch;

import java.util.List;

/**
 * Created by Thriveni on 5/15/2018.
 */

public class KeywordSearchResponse {
    private String response;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    private String success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    private List<SearchResult> searchResult;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<SearchResult> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(List<SearchResult> searchResult) {
        this.searchResult = searchResult;
    }

    @Override
    public String toString() {
        return "ClassPojo [response = " + response + ", searchResult = " + searchResult + "]";
    }
}
