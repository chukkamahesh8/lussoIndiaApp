package com.lussoindia.app;

import android.app.Application;

import com.lussoindia.network.RestApi;

/**
 * Created by mahesh on 11/8/2017.
 */

public class BaseApplication extends Application {
    private static BaseApplication mInstance;

    public static synchronized BaseApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //Initialise rest api
        RestApi.init(this);
        //FirebaseApp.initializeApp(this);
        //FirebaseInstanceId.getInstance().getToken();
    }

}
